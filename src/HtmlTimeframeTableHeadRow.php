<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Baut eine Zeile in der rechts die aktuelle Anzahl der Datensätze steht
 * und links ein Navielement zum durchblättern erscheint
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlTimeframeTableHeadRow extends HtmlTableHeadRow
{
    private $InputElements;
    private $TableProperties;

    /**
     * Creates a new html table head row
     */
    public function __construct(HtmlTableProperties $Properties)
    {
        $this->TableProperties = $Properties;
        $this->InputElements = new HtmlTimeframeInputElements();
        $this->InputElements->setTableIdent($this->TableProperties->getIdent());
        $this->InputElements->setTimeframe($this->TableProperties->getTimeframeArray());
        $this->InputElements->setActionLink(FrontController::getInstance()->getLinkTo());
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert das konfigurierbare Eingabeelement-Monster
     */
    public function getInputElements()
    {
        return $this->InputElements;
    }
    // End getInputElements

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds a single table row
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $DOMDocument)
    {
        if (!$this->TableProperties->getTimeframeState())
            return;

        return parent::build($DOMDocument);
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the table columns
     *
     * @param  array $columns
     */
    public function setColumns(array $columns)
    {
        parent::setColumns($columns);

        foreach($this->getColumns() as $name => $Column)
        {
            $Column->appendAttribute('colspan', count($this->getColumns()));
            $Column->appendAttribute('class', 'timeframe-tablehead');
            $ODiv = $Column->setOutputElement(new HtmlTag('div'));
            $ODiv->setAttribute('class', 'timeframe-outer');
            $ODiv->add($this->InputElements);
        }
    }
    // End setColumns

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableRow
