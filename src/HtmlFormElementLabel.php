<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;
use Verraes\ClassFunctions\ClassFunctions;

/**
 * This class implements a label decorator for a HtmlFormElement.
 *
 * The generated HTML looks something like that:
 *
 * <code>
 * <div>
 *   <div class="labelDiv">
 *      <label>Label</label>
 *   </div>
 *   <div class="elementDiv">
 *      content
 *   </div>
 * </div>
 * </code>
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class HtmlFormElementLabel extends HtmlElement
{
    /**
     * The label content for the element
     */
    private $label;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new element
     *
     * @param string $label the label text for the element
     * @param HtmlElement $DataElement the content element
     */
    public function __construct($label, HtmlElement $Element = null)
    {
        $this->label = $label;
        if (!is_null($Element)) {
            $this->addChild($Element);
        }
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $FirstFormElement = HtmlFormElement::searchFirstFormElementChild($this);

        $DivElt = $Document->createElement('div');

        $this->buildAndSetAttributes($DivElt);
        if($FirstFormElement)
        {
            $error = '';
            if ($FirstFormElement->hasError())
                $error = ' error';

            $formEltName = $FirstFormElement->getName();

            // clean from array
            if($pos = strpos($formEltName, '['))
                $formEltName = substr($formEltName, 0, $pos);

            $DivElt->setAttribute('class', trim($DivElt->getAttribute('class') . ' formSection labelElementDiv ' . ClassFunctions::short($FirstFormElement) . '-section ' . $formEltName . $error));
        }
        else
            $DivElt->setAttribute('class', trim($DivElt->getAttribute('class') . ' labelElementDiv'));

        $LabelDiv = $Document->createElement('div');

        $class = 'formEltLabel labelDiv';
        if($FirstFormElement)
            $class .= ' ' . ClassFunctions::short($FirstFormElement) . '-label';

        $LabelDiv->setAttribute('class', $class);

        $Label = $Document->createElement('label');

        /**
         * Connect label and first form element by id
         */
        if($FirstFormElement)
            $Label->setAttribute('for', $FirstFormElement->getId(true));

        $Label->appendChild($Document->createTextnode($this->label));
        $LabelDiv->appendChild($Label);

        $DivElt->appendChild($LabelDiv);

        $EltDiv = $Document->createElement('div');
        $EltDiv->setAttribute('class', 'formElt elementDiv');
        $DivElt->appendChild($EltDiv);

        foreach ($this->getChildren() as $Child) {
            $Child->appendTo($EltDiv);
            $EltDiv->setAttribute('class', trim($EltDiv->getAttribute('class') . ' ' . ClassFunctions::short($Child)));
        }

        return $DivElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlFormElementLabel
