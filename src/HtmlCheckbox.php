<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\HtmlTools\Interfaces\Converter;
use DOMDocument;


/**
 * A checkbox element for HtmlForms.
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlCheckbox extends HtmlFormElement
{
    /**
     * The caption of the checkbox
     */
    private $caption;

    /**
     * Standardmäßig wird die caption als \DOMText eingehängt.
     * steht maskHtml auf false wird evtl. in der Caption stehendes HTML auch als solches interpretiert.
     */
    private $maskHtml = true;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new checkbox
     *
     * @param string $name the name of the checkbox
     * @param string $value the value of the checkbox
     * @param string $caption the caption of the checkbox
     */
    public function __construct($name, $value = null, $caption = '', $readonly = false, Converter $DefaultConverter = null)
    {
        parent::__construct($name, $value, $readonly, $DefaultConverter);
        $this->caption = $caption;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Schalter für maskHtml.
     * maskHtml = true -> HTML-Tags werden maskiert.
     * maskHtml = false -> HTML wird intepretiert
     */
    public function setMaskHtml($mode)
    {
        $this->maskHtml = $mode;
    }
    // End setMaskHtml

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element.
     *
     * @see HtmlElement::build()
     * @param DOMDocument $Document
     * @return DOMNode|\DOMElement
     */
    public function build(DOMDocument $Document)
    {
        $DivElt = $Document->createElement('div');

        $InputElt = $Document->createElement('input');

        $InputElt->setAttribute('type', 'checkbox');
        $InputElt->setAttribute('name', $this->getName());

        if ($this->isReadonly()) {
            $InputElt->setAttribute('readonly', 'readonly');
        }
        if ($this->isDisabled()) {
            $InputElt->setAttribute('disabled', 'disabled');
        }

        $InputElt->setAttribute('value', 'true');
        if($this->getConvertedTextValue())
            $InputElt->setAttribute('checked', 'checked');

        /**
         * Set remaining attributes
         */
        $this->buildAndSetAttributes($InputElt, $this->getDataObject(), $this->getName());
        $DivElt->appendChild($InputElt);

        $SpanElt = $Document->createElement('span');
        if ($this->maskHtml)
        {
            $SpanElt->appendChild($Document->createTextnode($this->caption));
        }
        else
        {
            $View = new I18NHtmlView();
            $View->loadXml('<div id="TAKE_MY_CHILDS">' . $this->caption . '</div>');
            foreach($View->getElementById('TAKE_MY_CHILDS')->childNodes as $ChildNode)
                $SpanElt->appendChild($Document->importNode($ChildNode, true));
        }
        $DivElt->appendChild($SpanElt);

        return $DivElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlCheckbox
