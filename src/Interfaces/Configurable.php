<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools\Interfaces;

/**
 * Configurable interface
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
interface Configurable
{
    /**
     * Sets a config value
     *
     * @param string $name     the name of the config variable to set
     * @param mixed  $value    the value of the variable
     * @return Formatter       $this
     */
    public function set($name, $value);

    /**
     * Returns the configured value or the defaultValue
     *
     * @param  string $name           the name of the config variable to return
     * @param  mixed  $defaultValue   the default value to return if variable is empty
     * @return mixed                  the value
     */
    public function get($name, $defaultValue = null);

    /**
     * Checks if a config value exists
     *
     * @param string $name   the name of the config variable
     * @return boolean       true if it exists
     */
    public function has($name);

    /**
     * Returns all configured parameters
     *
     * @return array
     */
    public function getConfig();
}
// End Configurable
