<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools\Interfaces;

/**
 * The Converter interface.
 *
 * A converter is able to convert a value with the help of a given
 * DataObject and a property.
 * 
 * There are two directions to use this interface:
 *  - To convert an object and a property to a text value
 *  - To convert a text value and a property into an object
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
interface Converter
{
    /**
     * Converts the given value, DataObject and property to a text value
     *
     * @param string $value          The current value 
     * @param \stdClass $DataObject   The data object or null if none is available
     * @param string  $property      The property that was requested
     * @return string                the text value
     */
    public function convertToText($value, $DataObject = null, $property = null);

    /**
     * Thats the backwards function of convertToText
     *
     * @param string $value          The current value 
     * @param \stdClass $DataObject   The data object or null if none is available
     * @param string  $property      The property that was requested
     * @return mixed
     */
    public function convertFromText($value, $DataObject = null, $property = null);
}
// End Converter
