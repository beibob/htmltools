<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;
use DOMNode;

/**
 * The magic element! It can be every element you want it to be!
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class HtmlTag extends HtmlElement
{
    /**
     * Tag name
     */
    private $tagName;
    private $content;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the new element
     *
     * @param string $tagName
     */
    public function __construct($tagName, $content = null, array $attributes = [])
    {
        $this->tagName = $tagName;
        $this->content = $content;

        foreach($attributes as $key => $value)
            $this->setAttribute($key, $value);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element.
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $Elt = $Document->createElement($this->tagName);
        if (!is_null($this->content))
        {
            if ($this->content instanceof DOMNode)
                $Elt->appendChild($this->content);
            else
                $Elt->appendChild($Document->createTextNode($this->content));
        }
        $this->buildAndSetAttributes($Elt);

        foreach ($this->getChildren() as $Child) {
            $Child->appendTo($Elt);
        }

        return $Elt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTag
