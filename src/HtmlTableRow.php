<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * A html table row
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten M�rell <thorsten.muerell@beibob.net>
 *
 */
class HtmlTableRow extends HtmlElement
{
    /**
     * Table columns
     */
    protected $columns = [];

    /**
     * Table cell type
     */
    private $tableCellType = 'td';

    /**
     * row count
     */
    private $rowCount;

    /**
     * Current data object
     */
    private $DataObject;

    /**
     * Current row index
     */
    private $rowIndex;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the table columns by array. The array must specify in its key the property name
     * and in its value either a HtmlTableColumn object
     *
     * @param  array $columns
     */
    public function setColumns(array $columns)
    {
        /**
         * Copy table columns
         */
        $colCount = 0;
        $totalColumnCount = count($columns);

        foreach($columns as $name => $Column)
        {
            $this->columns[$name] = clone $Column;

            /**
             * Register this table row
             */
            $this->columns[$name]->setTableRow($this);

            /**
             * Mark first and last column
             */
            if($colCount == 0)
                $this->columns[$name]->appendAttribute('class', 'firstColumn');

            if($colCount == $totalColumnCount - 1)
                $this->columns[$name]->appendAttribute('class', 'lastColumn');

            $colCount++;
        }
    }
    // End setColumns

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the data cell type
     *
     * @param  string $type
     */
    public function setTableCellType($type = 'td')
    {
        $this->tableCellType = $type;
    }
    // End setTableCellType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the DataObject
     *
     * @param  object $DataObject
     * @return object the currently set object
     */
    public function setDataObject($DataObject)
    {
        return $this->DataObject = $DataObject;
    }
    // End setDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the DataObject
     *
     * @return object
     */
    public function getDataObject()
    {
        return $this->DataObject;
    }
    // End getDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the DataObject isset
     *
     * @return boolean
     */
    public function hasDataObject()
    {
        return is_object($this->DataObject);
    }
    // End hasDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the row count
     *
     * @param  int $rowCount
     */
    public function setRowCount($rowCount)
    {
        return $this->rowCount = $rowCount;
    }
    // End setRowCount

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the current row index
     *
     * @param  int $rowIndex
     * @return int the current set index
     */
    public function setRowIndex($rowIndex)
    {
        return $this->rowIndex = $rowIndex;
    }
    // End setRowIndex

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the row count
     *
     * @return  int
     */
    public function getRowCount()
    {
        return $this->rowCount;
    }
    // End getRowCount

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current row index
     *
     * @return  int
     */
    public function getRowIndex()
    {
        return $this->rowIndex;
    }
    // End getRowIndex

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a Column for the given name
     *
     * @return HtmlTableColumn
     */
    public function getColumn($name)
    {
        return $this->columns[$name];
    }
    // End getColumn

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns
     *
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }
    // End getColumns

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if this row has columns defined
     *
     * @return bool
     */
    public function hasColumns()
    {
        return count($this->columns) > 0;
    }
    // End hasColumns

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds a single table row
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $DOMDocument)
    {
        $rowIndex = $this->getRowIndex();
        $rowCount = $this->getRowCount();

        $TrElt = $DOMDocument->createElement('tr');

        /**
         * Build attributes
         */
        $DataObject = $this->hasDataObject() ? $this->DataObject : null;
        $this->buildAndSetAttributes($TrElt, $DataObject);

        /**
         * Append columns
         */
        $skipColumns = 0;
        foreach($this->getColumns() as $colName => $Column)
        {
            if($skipColumns-- > 0)
                continue;

            if($Column->hasColSpan())
                $skipColumns = ($Column->getColSpan() - 1);

            if(!$Column->hasDefinedTableCellType())
                $Column->setTableCellType($this->tableCellType);

            if($this->hasDataObject())
                $Column->setDataObject($this->DataObject);

            $Column->appendTo($TrElt);
        }

        return $TrElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableRow
