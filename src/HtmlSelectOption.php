<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Implements an option for the HtmlSelectbox
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlSelectOption extends HtmlFormElement
{
    /**
     * The caption of the select option
     */
    private $caption;

    /**
     * The value of the select option
     */
    private $value;

    /**
     * The parent select box
     */
    private $Selectbox;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new select option
     *
     * @param string $caption  the caption of the select option
     * @param string $value    the value of the select option
     */
    public function __construct($caption, $value) {
        $this->caption = $caption;
        $this->value = $value;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the caption
     *
     * @param  string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }
    // End setCaption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the value
     *
     * @param  string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
    // End setValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the select box
     *
     * @param  HtmlSelectBox $Selectbox the associated select box
     */
    public function setSelectbox(HtmlSelectbox $Selectbox)
    {
        $this->Selectbox = $Selectbox;
    }
    // End setSelectbox

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the selectbox
     *
     * @return HtmlSelectbox the associated html select box
     */
    public function getSelectbox()
    {
        return $this->Selectbox;
    }
    // End getSelectbox

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $OptionElt = $Document->createElement('option');
        $OptionElt->setAttribute('value', $this->value);
        $OptionElt->appendChild($Document->createTextnode($this->caption));

        if (is_object($this->Selectbox))
        {
            $value = $this->Selectbox->getConvertedTextValue();

            if (is_array($value) && $this->Selectbox instanceof HtmlMultiSelectbox) {
                if (in_array($this->value, $value, false)) {
                    $OptionElt->setAttribute('selected', 'selected');
                }
            }
            else {
                if( (string)$value == (string)$this->value) {
                    $OptionElt->setAttribute('selected', 'selected');
                }
            }
        }

        /**
         * Set remaining attributes
         */
        $this->buildAndSetAttributes($OptionElt, $this->getDataObject(), $this->getName());

        return $OptionElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    // Disable this for a select element
    public function getValidatedFormDataObject() { return null; }

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlSelectOption
