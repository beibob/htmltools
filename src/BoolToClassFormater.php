<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;


/**
 * This is a simple formatter which converts a bool to classes true or false
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 * @abstract
 *
 */
class BoolToClassFormater extends AbstractFormatter
{
    private $titels = array();

    /**
     * Creates a new instance
     *
     * @param array $values   an array of values
     * @param string $attr    the attribute to set
     * @param $append         if the value should be appended
     */
    public function __construct($values = array('false', 'true'), $attr = "class", $append = false, $titles = array())
    {
        $this->attr = $attr;
        $this->values = $values;
        $this->append = $append;

        $this->titles = $titles;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Formats the object
     *
     * @see Formatter::format()
     */
    public function format(HtmlElement $obj, $DataObject = null, $property = null)
    {
        $boolToInt = (bool) $DataObject->$property;
        $boolToInt = (int) $boolToInt;

        if ($this->append)
            $obj->appendAttribute($this->attr, $this->values[$boolToInt]);
        else
            $obj->setAttribute($this->attr, $this->values[$boolToInt]);

        if (isset($this->titles[$boolToInt]))
        {
            if ($this->append)
                $obj->appendAttribute('title', $this->titles[$boolToInt]);
            else
                $obj->setAttribute('title', $this->titles[$boolToInt]);
        }
    }
    // End format

    //////////////////////////////////////////////////////////////////////////////////////
}
// End BoolToClassFormater
