<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 * Formater building an open or closed flap.
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class OpenCloseSwitchFormater extends AbstractFormatter
{
    /**
     * List of open elements
     */
    private $openObjectIds = array();

    private $onClick;

    /**
     * Sets the list of open objects
     */
    public function __construct(array $openObjectIds = array(), $onClick = null)
    {
        $this->openObjectIds = $openObjectIds;

        if (!is_null($onClick))
            $this->onClick = $onClick;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Formats the object
     *
     * @see Formatter::format()
     */
    public function format(HtmlElement $obj, $DataObject = null, $property = null)
    {
        $css = in_array($DataObject->$property, $this->openObjectIds) ? 'openClose open' : 'openClose closed';
        $obj->setAttribute('class', $css);

        if (!is_null($this->onClick))
            $obj->setAttribute('onclick', sprintf($this->onClick, $DataObject->$property));
    }
    // End format

    //////////////////////////////////////////////////////////////////////////////////////
}
// End OpenCloseSwitchFormater
