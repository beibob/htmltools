<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * This creates a grouping element for HtmlSelectOption elements
 *
 * <code>
 *   $OptGroup = $Select->add(new HtmlSelectOptGroup('Label'));
 *   $OptGroup->add(new HtmlSelectOption))
 * </code>
 *
 * @package blibs
 * @author Tobias Lode <tobias.lode@beibob.de>
 *
 */
class HtmlSelectOptGroup extends HtmlFormElement
{
    /**
     * The caption of the select option
     */
    private $caption;

    /**
     * The parent select box
     */
    private $Selectbox;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new select optgroup
     *
     * @param string $caption  the caption of the select option
     */
    public function __construct($caption)
    {
        $this->caption = $caption;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the caption
     *
     * @param  string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }
    // End setCaption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the select box
     *
     * @param  HtmlSelectBox $Selectbox the associated select box
     */
    public function setSelectbox(HtmlSelectbox $Selectbox)
    {
        $this->Selectbox = $Selectbox;
    }
    // End setSelectbox

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the selectbox
     *
     * @return HtmlSelectbox the associated html select box
     */
    public function getSelectbox()
    {
        return $this->Selectbox;
    }
    // End getSelectbox

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $OptGroup = $Document->createElement('optgroup');
        $OptGroup->setAttribute('label', $this->caption);

        $this->buildAndSetAttributes($OptGroup);

        foreach ($this->getChildren() as $Child)
            $Child->appendTo($OptGroup);

        return $OptGroup;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a child to this SelectBox.
     *
     * Note: Only children of type HtmlSelectOption can be added.
     *
     * @param  HtmlElement $Element the element to add to this radio group
     * @return HtmlElement the added element
     */
    public function addChild(HtmlElement $Element)
    {
        if (!$Element instanceof HtmlSelectOption)
            throw new Exception("Cannot add anything other than an option element to a HtmlSelectOptGroup.");

        $Element->setSelectbox($this->getSelectbox());
        return parent::addChild($Element);
    }
    // End addChild

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlFormGroup
