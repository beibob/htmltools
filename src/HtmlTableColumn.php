<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Implements a table column for use in HtmlTables and HtmlRows
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten M�rell <thorsten.muerell@beibob.net>
 *
 */
class HtmlTableColumn extends HtmlDataElement
{
    /**
     * Default table cell type
     */
    const DEFAULT_TABLE_CELL_TYPE = 'td';

    /**
     * Column caption
     */
    private $caption;

    /**
     * Table cell type
     */
    private $tableCellType;

    /**
     * Column span
     */
    private $colSpan;

    /**
     * TableRow
     */
    private $TableRow;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new table column
     *
     * @param  string $name                    the name of the column (this is the identifier)
     * @param  string $caption                 the caption of the column
     * @param  HtmlDataElement $OutputElement  the output element to use in this column
     */
    public function __construct($name, $caption = null, HtmlDataElement $OutputElement = null)
    {
        $this->setName($name);
        $this->setCaption($caption);
        if (is_null($OutputElement))
            $this->setOutputElement(new HtmlText($name));
        else
            $this->setOutputElement($OutputElement);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the caption for the column
     *
     * @param  string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = (string)$caption;
    }
    // End setCaption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the output element
     *
     * @param  HTMLDataElement $OutputElement  the output element
     * @return HtmlDataElement                 the just added element
     */
    public function setOutputElement(HTMLElement $OutputElement)
    {
        $this->clearChildren();
        return $this->addChild($OutputElement);
    }
    // End setOutputElement

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the table row
     *
     * @param  HtmlTableRow $TableRow
     */
    public function setTableRow(HtmlTableRow $TableRow)
    {
        $this->TableRow = $TableRow;
    }
    // End setTableRow

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the data cell type
     *
     * @param  string $type
     */
    public function setTableCellType($type = 'td')
    {
        $this->tableCellType = (string)$type;
    }
    // End setTableCellType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the table cell type is already defined
     *
     * @param  string $type
     */
    public function hasDefinedTableCellType()
    {
        return isset($this->tableCellType);
    }
    // End hasDefinedTableCellType

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the column caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }
    // End getCaption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets a colspan
     *
     * @param  int    $colSpan
     * @return -
     */
    public function setColSpan($colSpan)
    {
        $this->setAttribute('colspan', $colSpan);
    }
    // End setColSpan

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if col span is defined
     *
     * @return boolean if the colspan is defined
     */
    public function hasColSpan()
    {
        return $this->hasAttribute('colspan') && $this->getAttribute('colspan') > 0;
    }
    // End hasColSpan

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the col span
     *
     * @return int
     */
    public function getColSpan()
    {
        return $this->getAttribute('colspan');
    }
    // End getColSpan

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds a single table cell for the column
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $DOMDocument)
    {
        $property = $this->getName();

        /**
         * Build td or th element
         */
        $DataElt = $DOMDocument->createElement($this->hasDefinedTableCellType()? $this->tableCellType : self::DEFAULT_TABLE_CELL_TYPE);

        /**
         * Build attributes
         */
        $this->buildAndSetAttributes($DataElt, $this->getDataObject(), $property);

        /**
         * Build cell content
         */
        HtmlDataElement::setDataObjectRecursive($this, $this->getDataObject());

        foreach ($this->getChildren() as $Child) {
            $Child->appendTo($DataElt);
        }

        return $DataElt;
    }
    // End build
}
// End HtmlTableColumn
