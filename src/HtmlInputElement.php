<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Html input form element
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 * @abstract
 *
 */
abstract class HtmlInputElement extends HtmlFormElement
{
    /**
     * Input type
     */
    private $type;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the type
     *
     * @return string
     */
     public function getType()
     {
         return $this->type;
     }
     // End getType

     //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the type
     *
     */
     public function setType($type)
     {
         return $this->type = $type;
     }
     // End setType

     //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $InputElt = $Document->createElement('input');

        $InputElt->setAttribute('type', $this->type);
        $InputElt->setAttribute('name', $this->getName());

        if ($this->isReadonly()) {
            $InputElt->setAttribute('readonly', 'readonly');
        }
        if ($this->isDisabled()) {
            $InputElt->setAttribute('disabled', 'disabled');
        }
        $InputElt->setAttribute('value', $this->getConvertedTextValue());

        /**
         * Set remaining attributes
         */
        $this->buildAndSetAttributes($InputElt, $this->getDataObject(), $this->getName());

        return $InputElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlInputElement
