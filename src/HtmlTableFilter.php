<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 *
 */
abstract class HtmlTableFilter
{
    private $filterProperty;
    protected $filterValue;
    private $TableProperties;
    protected $dataType = PDO::PARAM_STR;

    abstract public function appendTo($Container);

    public function __construct($property, $value = null, $dataType = null)
    {
        $this->setFilterProperty($property);
        if (!is_null($value))
            $this->setFilterValue($value, HtmlTableProperties::SET_FILTERVALUE_CONTEXT_CONSTRUCT);

        if (!is_null($dataType))
            $this->setDataType($dataType);
           else
               $this->setDataType(PDO::PARAM_STR);
    }

    public function setFilterProperty($property)
    {
        return $this->filterProperty = $property;
    }
    public function getFilterProperty()
    {
        return $this->filterProperty;
    }
    public function setFilterValue($value, $context = HtmlTableProperties::SET_FILTERVALUE_CONTEXT_UNKNOWN)
    {
        return $this->filterValue = $value;
    }
    public function getFilterValue()
    {
        return $this->filterValue;
    }
    public function getFilterSql()
    {
        if (!is_null($this->getFilterValue()))
        {
            switch ($this->dataType)
            {
                case PDO::PARAM_BOOL:
                    $bool = (bool) $this->getFilterValue() ? 'true' : 'false';
                    return StringFactory::unCamelCase($this->getFilterProperty()) . "=" . $bool;
                break;
                case PDO::PARAM_INT:
                    return StringFactory::unCamelCase($this->getFilterProperty()) . "=" . (int) $this->getFilterValue();
                break;
                case PDO::PARAM_STR:
                default:
                    return StringFactory::unCamelCase($this->getFilterProperty()) . "='" . $this->getFilterValue() . "'";
                break;
            }
        }

        return false;
    }
    public function setTableProperties(HtmlTableProperties $TableProperties)
    {
        return $this->TableProperties = $TableProperties;
    }
    public function getTableProperties()
    {
        return $this->TableProperties;
    }
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;
    }
}
// End
