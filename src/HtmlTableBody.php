<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\DataObjectSet;
use DOMDocument;

/**
 * Implements a html table body.
 *
 * This is just a container for table rows.
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlTableBody extends HtmlTableRowGroup
{
    /**
     * The data set to build the rows with
     */
    private $DataSet = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new table body
     *
     * @param  HtmlTable $Table the parent table
     */
    public function __construct(HtmlTable $Table)
    {
        parent::__construct($Table, 'tbody');
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the data set. This is an array of objects
     *
     * @param  array
     */
    public function setDataSet($DataSet)
    {
        return $this->DataSet = $DataSet;
    }
    // End setDataSet

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the data set. This is an array of objects
     *
     * @return array
     */
    public function getDataSet()
    {
        return $this->DataSet;
    }
    // End getDataSet

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $DOMDocument)
    {
        if(!$RowGroupElt = $this->getRowGroupElt($DOMDocument))
            return null;

        /**
         * Iterate over the data set
         */
        $DataSet = $this->getDataSet();

        $rowCounter = 0;
        $rowCount   = count($DataSet);
        $alternateRowCount = count($this->tableRows);

        if(is_array($DataSet) || $DataSet instanceof DataObjectSet)
        {
            foreach($DataSet as $DataObject)
            {
                /**
                 * Alternate rows
                 */
                $TableRow = clone $this->tableRows[$rowCounter % $alternateRowCount];
                $TableRow->setRowCount($rowCount);
                $TableRow->setRowIndex($rowCounter);
                $TableRow->setDataObject($DataObject);

                if($rowCounter == 0)
                    $TableRow->addClass('firstRow');

                if($rowCounter == $rowCount - 1)
                    $TableRow->addClass('lastRow');

                $TableRow->appendTo($RowGroupElt);

                $rowCounter++;
            }
        }

        return $RowGroupElt;
    }
    // End build
}
// End HtmlTableBody
