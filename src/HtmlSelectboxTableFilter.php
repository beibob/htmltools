<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

class HtmlSelectboxTableFilter extends HtmlTableFilter
{
    private $options = array();

    public function appendTo($Container)
    {
        $Selectbox = $Container->add(new HtmlSelectbox('filter[' . $this->getFilterProperty() . ']', $this->getFilterValue()));
        $Selectbox->setAttribute('onchange', 'submit()');
        foreach ($this->options as $value => $caption)
            $Selectbox->addChild(new HtmlSelectOption($caption, (string) $value));
    }
    public function addOption($value, $caption)
    {
        $this->options[$value] = $caption;
    }
    public function setOptions(array $options = array())
    {
        $this->options = $options;
    }
    public function getFilterSql()
    {
        if (!is_null($this->getFilterValue()) && $this->getFilterValue() != 'NULL')
        {
            switch ($this->dataType)
            {
                case PDO::PARAM_BOOL:
                    $bool = (bool) $this->getFilterValue() ? 'true' : 'false';
                    return StringFactory::unCamelCase($this->getFilterProperty()) . "=" . $bool;
                break;
                case PDO::PARAM_INT:
                    return StringFactory::unCamelCase($this->getFilterProperty()) . "=" . (int) $this->getFilterValue();
                break;
                case PDO::PARAM_STR:
                default:
                    return StringFactory::unCamelCase($this->getFilterProperty()) . "='" . $this->getFilterValue() . "'";
                break;
            }
        }

        return false;
    }
}
// End HtmlSelectboxTableFilter
