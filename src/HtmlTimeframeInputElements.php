<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\BlibsDate;
use Beibob\Blibs\IdFactory;
use DOMDocument;

/**
 * Baut Eingabeelemente um eine Tabelle nach einer Spalte
 * zeitlich einzugrenzen
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class HtmlTimeframeInputElements extends HtmlElement
{
    /**
     * Wer mag kann hier einen Sprachcatalog registrieren
     * Wenn das nicht kommt, wirds recht deutsch!
     */
    public static $I18nCatalog = null;

    /**
     * Bei der Zeitlichen Eingrenzung stehen drei Modi zur Auswahl
     */
    const MODE_DAY            = 'OneDay';
    const MODE_MONTH          = 'OneMonth';
    const MODE_YEAR           = 'OneYear';
    const MODE_DAY_TO_DAY     = 'DayToDay';
    const MODE_MONTH_TO_MONTH = 'MonthToMonth';
    const MODE_YEAR_TO_YEAR   = 'YearToYear';

    /**
     * Eindeutiger Bezeichner der Tabelle f�r den der
     * Limter zust�ndig ist
     */
    private $tableIdent;

    /**
     * ActionLink
     */
    private $actionLink;

    /**
     * Gibt es in der Tabelle eine Filterzeile?
     */
    private $timeframeEnabled = false;

    /**
     * Status der Filterzeile in der Tabelle
     */
    private $timeframeState = true;

    /**
     * aktuelles Zeitfenster
     */
    private $timeframe = [];

    /**
     * aktuelles Zeitfenster
     */
    private $selectMode = self::MODE_DAY_TO_DAY;

    /**
     * Jahre die der oder den Jahres-Auswahlboxen w�hlbar sind
     */
    private $selectableYears = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt den TableIdent
     */
    public function setTableIdent($ident)
    {
        return $this->tableIdent = $ident;
    }
    // End setTableIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert den TableIdent
     */
    public function getTableIdent()
    {
        return $this->tableIdent;
    }
    // End getTableIdent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the action links
     *
     * @param  string $actionLink
     * @return -
     */
    public function setActionLink($link)
    {
        return $this->actionLink = $link;
    }
    // End setActionLink

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the action links
     *
     * @param  string $actionLink
     * @return -
     */
    public function getActionLink()
    {
        return $this->actionLink;
    }
    // End getActionLink

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt den Auswahlmodus
     */
    public function setSelectMode($mode)
    {
        return $this->selectMode = $mode;
    }
    // End setSelectMode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert den Auswahlmodus
     */
    public function getSelectMode()
    {
        return $this->selectMode;
    }
    // End getSelectMode

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Schaltet die Funtionen zum limitieren der Tabelle ein
     */
    public function enableTimeframe()
    {
        $this->timeframeEnabled = true;
    }
    // End enableTimeframe

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Schaltet die Funtionen zum limitieren der Tabelle aus
     */
    public function disableTimeframe()
    {
        $this->timeframeEnabled = false;
    }
    // End disableTimeframe

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert den Zustand des Schalters timeframeEnabled
     */
    public function getTimeframeEnabled()
    {
        return $this->timeframeEnabled;
    }
    // End getTimeframeEnabled

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets active timeframe
     *
     * @param  array $timeframe - array('start' => yyyy-mm-dd, 'stop' => yyyy-mm-dd);
     * @return array
     */
    public function setTimeframe(array $timeframe)
    {
        if (!isset($timeframe['start']))
            $timeframe['start'] = '';

        if (!isset($timeframe['stop']))
            $timeframe['stop'] = '';

        return $this->timeframe = $timeframe;
    }
    // End setTimeframe

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets active limit
     *
     * @param  int $limit
     * @return int
     */
    public function getTimeframe()
    {
        return $this->timeframe;
    }
    // End getTimeframe

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt den aktuellen Zustand des Zeitraumeingrenzers
     *
     * @param  int $limit
     * @return int
     */
    public function setTimeframeState($state)
    {
        return $this->timeframeState = $state;
    }
    // End setTimeframeState

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert den aktuellen Zustand des Zeitraumeingrenzers
     *
     * @param  int $limit
     * @return int
     */
    public function getTimeframeState()
    {
        return $this->timeframeState;
    }
    // End getTimeframeState

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert die in einer Jahres-Auswahlbox zur Verf�gung stehenden Jahre
     */
    public function getSelectableYears($startOrStop)
    {
        if (!isset($this->selectableYears[$startOrStop]))
        {
            $this->selectableYears[$startOrStop] = [];
            for ($i = 2000; $i <= 2020; $i++)
                $this->selectableYears[$startOrStop][$i] = $i;
        }

        return $this->selectableYears[$startOrStop];
    }
    // End getSelectableYears

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt die in einer Jahres-Auswahlbox zur Verfügung stehenden Jahre
     */
    public function setSelectableYears(array $range, $startOrStop = null)
    {
        if ($startOrStop)
            $this->selectableYears[$startOrStop] = $range;
        else
            $this->selectableYears['start'] = $this->selectableYears['stop'] = $range;
    }
    // End setSelectableYears

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value of the property
     *
     * @param  DOMElement $DOMElement
     * @param  \stdClass $DataObject
     * @return DOMElement
     */
    public function build(DOMDocument $Document)
    {
        if (!$this->getTimeframeState())
            return $Document->createTextNode('');

        $timeframe = $this->getTimeframe();
        $DTStart = new BlibsDate($timeframe['start']);
        $DTStop = new BlibsDate($timeframe['stop']);

        // Innerer Container
        $Container = $Document->createElement('div');
        $Container->setAttribute('class', 'htmlTimeframeHead');

        $this->appendInputElements($Container);

        // Clearing
        $Container->appendChild($Document->createElement('div', '', ['class' => 'clearing']));
        return $Container;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Ruft je nach SelectMode eine der buildXXXInputElement auf
     * und hängt ein hidden-Feld mit dem aktiven SelectMode ein
     */
    protected function appendInputElements(DOMElement $Container)
    {
        $timeframe = $this->getTimeframe();
        $DTStart = new BlibsDate($timeframe['start']);
        $DTStop = new BlibsDate($timeframe['stop']);

        $method = 'build' . $this->getSelectMode() . 'InputElement';

        $this->$method($Container, $DTStart, $DTStop);

        $Hidden = $Container->appendChild($Container->ownerDocument->createElement('input'));
        $Hidden->setAttribute('type', 'hidden');
        $Hidden->setAttribute('name', 'timeframeMode');
        $Hidden->setAttribute('value', $this->getSelectMode());
    }
    // End appendInputElements

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Abschicken Link
     */
    protected function appendSubmitLink($Container, $caption = null)
    {
        if (is_null($caption))
            $caption =  self::getI18NText('submit_link_caption_default');

        $Label = $Container->appendChild($Container->ownerDocument->createElement('div', '', ['class' => 'tfLabel']));

        $linkAttributes = [];
        $linkAttributes['href'] = '#';
        $linkAttributes['onclick'] = "blibsForm.setActionAndSubmit(event, '" . $this->getActionLink() . "', blibsForm.getParentFormByElement(this).name);return false;";
        $Label->appendChild($Container->ownerDocument->createElement('a', $caption, $linkAttributes));
    }
    // End appendSubmitLink

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected function buildOneDayInputElement($Container, $Start, $Stop)
    {
        $DOMDocument = $Container->ownerDocument;

        // Innerer Container
        $Inner = $Container->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfInner']));

        // Tag:
        $Label = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfLabel']));
        $Label->appendChild($DOMDocument->getSpan(self::getI18NText('input-label-day')));

        // Eingabe Start
        $Input = $Inner->appendChild($DOMDocument->createElement('input'));
        $Input->setAttribute('type', 'text');
        $Input->setAttribute('name', 'timeframe_start');
        $Input->setAttribute('class', 'start');
        $Input->setAttribute('value', $Start->isValid() ? $Start->getDateString('d.m.Y') : '');

        // Eingrenzen-Link
        $this->appendSubmitLink($Inner, self::getI18NText('submit_link_caption_choose'));
    }
    // End buildInputElementOneDay

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Baut die Eingabefelder zur Auswahl eines Monats
     */
    protected function buildOneMonthInputElement($Container, $Start, $Stop)
    {
        $DOMDocument = $Container->ownerDocument;

        // Innerer Container
        $Inner = $Container->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfInner']));

        // Monat:
        $Label = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfLabel']));
        $Label->appendChild($DOMDocument->getSpan(self::getI18NText('input-label-month'), ['id' => IdFactory::getUniqueXmlId()]));

        // Selectbox Monat
        $SelectBoxDiv = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfSelectboxMonth']));
        $Selectbox = new HtmlJsSelectbox('timeframe_start_m', (int) $Start->getMonth());
        $Selectbox->addOption('NULL', self::getI18NText('null-option-start-month'));
        foreach (BlibsDate::getMonthMapping('en') as $month => $monthName)
            $Selectbox->addOption((int) $month, $monthName);
        $Selectbox->setSelectedByValue((int) $Start->getMonth());
        $Selectbox->appendTo($SelectBoxDiv);

        // Selectbox Jahr
        $SelectBoxDiv = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfSelectboxYear']));
        $Selectbox = new HtmlJsSelectbox('timeframe_start_y', (int) $Start->getYear());
        $Selectbox->addOption('NULL', self::getI18NText('null-option-start-year'));
        foreach ($this->getSelectableYears('start') as $year)
            $Selectbox->addOption((int) $year, $year);
        $Selectbox->setSelectedByValue((int) $Start->getYear());
        $Selectbox->appendTo($SelectBoxDiv);

        // Eingrenzen-Link
        $this->appendSubmitLink($Inner, self::getI18NText('submit_link_caption_choose'));
    }
    // End buildOneMonthInputElement

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected function buildOneYearInputElement($Container, $Start, $Stop)
    {
        $DOMDocument = $Container->ownerDocument;

        // Innerer Container
        $Inner = $Container->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfInner']));

        // Jahr:
        $Label = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfLabel']));
        $Label->appendChild($DOMDocument->getSpan(self::getI18NText('input-label-year'), ['id' => IdFactory::getUniqueXmlId()]));

        // Selectbox
        $SelectBoxDiv = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfSelectboxYear']));
        $Selectbox = new HtmlJsSelectbox('timeframe_start_y', (int) $Start->getYear());
        $Selectbox->addOption('NULL', self::getI18NText('null-option-start-year'));
        foreach ($this->getSelectableYears('start') as $year)
            $Selectbox->addOption((int) $year, $year);

        $timeframe = $this->getTimeframe();
        $selected = $timeframe['start'] ? (int) $Start->getYear() : 'NULL';

        $Selectbox->setSelectedByValue($selected);
        $Selectbox->appendTo($SelectBoxDiv);

        // Eingrenzen-Link
        $this->appendSubmitLink($Inner, self::getI18NText('submit_link_caption_choose'));
    }
    // End buildOneYearInputElement

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected function buildDayToDayInputElement($Container, $Start, $Stop)
    {
        $DOMDocument = $Container->ownerDocument;

        // Innerer Container
        $Inner = $Container->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfInner']));

        // Von:
        $Label = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfLabel']));
        $Label->appendChild($DOMDocument->getSpan(self::getI18NText('input-label-from')));

        // Eingabe Start
        $Input = $Inner->appendChild($DOMDocument->createElement('input'));
        $Input->setAttribute('type', 'text');
        $Input->setAttribute('name', 'timeframe_start');
        $Input->setAttribute('class', 'start');
        $Input->setAttribute('value', $Start->isValid() ? $Start->getDateString('d.m.Y') : '');

        // Bis:
        $Label = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfLabel']));
        $Label->appendChild($DOMDocument->getSpan(self::getI18NText('input-label-to')));

        // Eingabe Stop
        $Input = $Inner->appendChild($DOMDocument->createElement('input'));
        $Input->setAttribute('type', 'text');
        $Input->setAttribute('name', 'timeframe_stop');
        $Input->setAttribute('class', 'stop');
        $Input->setAttribute('value', $Stop->isValid() ? $Stop->getDateString('d.m.Y') : '');

        // Eingrenzen-Link
        $this->appendSubmitLink($Inner);
    }
    // End buildDayToDayInputElement

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected function buildMonthToMonthInputElement($Container, $Start, $Stop)
    {
        $DOMDocument = $Container->ownerDocument;

        // Innerer Container
        $Inner = $Container->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfInner']));

        // Von:
        $Label = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfLabel']));
        $Label->appendChild($DOMDocument->getSpan(self::getI18NText('input-label-from')));

        // Start Monat
        $SelectBoxDiv = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfSelectboxMonth']));
        $Selectbox = new HtmlJsSelectbox('timeframe_start_m');
        $Selectbox->addOption('NULL', self::getI18NText('null-option-start-month'));
        foreach (BlibsDate::getMonthMapping('en') as $month => $monthName)
            $Selectbox->addOption((int) $month, $monthName);
        $Selectbox->setSelectedByValue((int) $Start->getMonth());
        $Selectbox->appendTo($SelectBoxDiv);

        // Start Jahr
        $SelectBoxDiv = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfSelectboxYear']));
        $Selectbox = new HtmlJsSelectbox('timeframe_start_y');
        $Selectbox->addOption('NULL', self::getI18NText('null-option-start-year'));
        foreach ($this->getSelectableYears('start') as $year)
            $Selectbox->addOption((int) $year, $year);
        $Selectbox->setSelectedByValue((int) $Start->getYear());
        $Selectbox->appendTo($SelectBoxDiv);

        // Bis:
        $Label = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfLabel']));
        $Label->appendChild($DOMDocument->getSpan(self::getI18NText('input-label-to')));

        // Stop Monat
        $SelectBoxDiv = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfSelectboxMonth']));
        $Selectbox = new HtmlJsSelectbox('timeframe_stop_m');
        $Selectbox->addOption('NULL', self::getI18NText('null-option-stop-month'));
        foreach (BlibsDate::getMonthMapping('en') as $month => $monthName)
            $Selectbox->addOption((int) $month, $monthName);
        $Selectbox->setSelectedByValue((int) $Stop->getMonth());
        $Selectbox->appendTo($SelectBoxDiv);

        // Stop Jahr
        $SelectBoxDiv = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfSelectboxYear']));
        $Selectbox = new HtmlJsSelectbox('timeframe_stop_y');
        $Selectbox->addOption('NULL', self::getI18NText('null-option-stop-year'));
        foreach ($this->getSelectableYears('stop') as $year)
            $Selectbox->addOption((int) $year, $year);
        $Selectbox->setSelectedByValue((int) $Stop->getYear());
        $Selectbox->appendTo($SelectBoxDiv);

        // Eingrenzen-Link
        $this->appendSubmitLink($Inner);
    }
    // End buildMonthToMonthInputElement

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected function buildYearToYearInputElement($Container, $Start, $Stop)
    {
        $DOMDocument = $Container->ownerDocument;

        // Innerer Container
        $Inner = $Container->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfInner']));

        // Von:
        $Label = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfLabel']));
        $Label->appendChild($DOMDocument->getSpan(self::getI18NText('input-label-from')));

        // Start Jahr
        $SelectBoxDiv = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfSelectboxYear']));
        $Selectbox = new HtmlJsSelectbox('timeframe_start_y');
        $Selectbox->addOption('NULL', self::getI18NText('null-option-start-year'));
        foreach ($this->getSelectableYears('start') as $year)
            $Selectbox->addOption((int) $year, $year);
        $Selectbox->setSelectedByValue((int) $Start->getYear());
        $Selectbox->appendTo($SelectBoxDiv);

        // Bis:
        $Label = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfLabel']));
        $Label->appendChild($DOMDocument->getSpan(self::getI18NText('input-label-to')));

        // Stop Jahr
        $SelectBoxDiv = $Inner->appendChild($DOMDocument->createElement('div', '', ['class' => 'tfSelectboxYear']));
        $Selectbox = new HtmlJsSelectbox('timeframe_stop_y');
        $Selectbox->addOption('NULL', self::getI18NText('null-option-stop-year'));
        foreach ($this->getSelectableYears('stop') as $year)
            $Selectbox->addOption((int) $year, $year);
        $Selectbox->setSelectedByValue((int) $Stop->getYear());
        $Selectbox->appendTo($SelectBoxDiv);

        // Eingrenzen-Link
        $this->appendSubmitLink($Inner);
    }
    // End buildYearToYearInputElement

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein DateTime-Objekt mit dem aus der Nutzereingabe resultierenden Startdatum
     */
    public static function getDateTimeObjectsByRequest(HttpRequest $Request)
    {
        $dateTimeObjects = ['start' => false, 'stop' => false, 'errors' => []];

        if (!isset($Request->timeframeMode))
        {
            $dateTimeObjects['errors']['timeframe'] = self::getI18NText('inputerror-unknown-mode');
            return $dateTimeObjects;
        }

        switch ($Request->timeframeMode)
        {
            case self::MODE_DAY:
                $datePartsStart = self::validateDateInput($Request->timeframe_start);
                if (!$datePartsStart)
                {
                    $dateTimeObjects['errors']['start'] = self::getI18NText('inputerror-invalid-date');
                    return $dateTimeObjects;
                }

                $datePartsStart['hour'] = 0;
                $datePartsStart['minutes'] = 0;
                $datePartsStart['seconds'] = 0;
                $dateTimeObjects['start'] = new BlibsDateTime($datePartsStart);

                $datePartsStart['hour'] = 23;
                $datePartsStart['minutes'] = 59;
                $datePartsStart['seconds'] = 59;
                $dateTimeObjects['stop'] = new BlibsDateTime($datePartsStart);

                if (!$dateTimeObjects['start']->isValid() || !$dateTimeObjects['stop']->isValid())
                {
                    $dateTimeObjects['errors']['start'] = "Bitte geben Sie im Feld 'Tag' ein gültiges Datum ein";
                    $dateTimeObjects['errors']['start'] = self::getI18NText('inputerror-invalid-date');
                    return $dateTimeObjects;
                }
            break;
            case self::MODE_MONTH:
                if ($Request->timeframe_start_m == 'NULL' && $Request->timeframe_start_y == 'NULL')
                {
                    $dateTimeObjects['start'] = $dateTimeObjects['stop'] = null;
                    return $dateTimeObjects;
                }

                if (!isset($Request->timeframe_start_m) || $Request->timeframe_start_m == 'NULL')
                    $dateTimeObjects['errors']['start'] = self::getI18NText('inputerror-month-missing');

                if (!isset($Request->timeframe_start_y) || $Request->timeframe_start_y == 'NULL')
                    $dateTimeObjects['errors']['start'] = self::getI18NText('inputerror-month-missing');

                if (isset($dateTimeObjects['errors']['start']))
                    return $dateTimeObjects;

                $dateTimeObjects['start'] = new BlibsDateTime(sprintf('%s-%s-01 00:00:00'
                                                                      , $Request->timeframe_start_y
                                                                      , str_pad($Request->timeframe_start_m, 2, '0', STR_PAD_LEFT)));

                $dateTimeObjects['stop'] = new BlibsDateTime(sprintf('%s-%s-%s 23:59:59'
                                                                     , $Request->timeframe_start_y
                                                                     , str_pad($Request->timeframe_start_m, 2, '0', STR_PAD_LEFT)
                                                                     , BlibsDate::factory($dateTimeObjects['start']->getUnixtime())->getLastDayOfMonth()));
            break;
            case self::MODE_YEAR:
                if ($Request->timeframe_start_y == 'NULL')
                {
                    $dateTimeObjects['start'] = $dateTimeObjects['stop'] = null;
                    return $dateTimeObjects;
                }

                $dateTimeObjects['start'] = new BlibsDateTime(sprintf('%s-01-01 00:00:00'
                                                                      , $Request->timeframe_start_y));

                $dateTimeObjects['stop'] = new BlibsDateTime(sprintf('%s-12-31 23:59:59'
                                                                     , $Request->timeframe_start_y));

            break;
            case self::MODE_DAY_TO_DAY:
                if (trim($Request->timeframe_start . $Request->timeframe_stop) == '')
                {
                    $dateTimeObjects['start'] = $dateTimeObjects['stop'] = null;
                    return $dateTimeObjects;
                }

                $datePartsStart = self::validateDateInput($Request->timeframe_start);
                $datePartsStop = self::validateDateInput($Request->timeframe_stop);
                if (!$datePartsStart)
                    $dateTimeObjects['errors']['start'] = self::getI18NText('inputerror-startday-missing');

                if (!$datePartsStop)
                    $dateTimeObjects['errors']['stop'] = self::getI18NText('inputerror-stopday-missing');

                if (count($dateTimeObjects['errors']))
                    return $dateTimeObjects;

                $datePartsStart['hour'] = 0;
                $datePartsStart['minutes'] = 0;
                $datePartsStart['seconds'] = 0;
                $dateTimeObjects['start'] = new BlibsDateTime($datePartsStart);

                $datePartsStop['hour'] = 23;
                $datePartsStop['minutes'] = 59;
                $datePartsStop['seconds'] = 59;
                $dateTimeObjects['stop'] = new BlibsDateTime($datePartsStop);

                if (!$dateTimeObjects['start']->isValid())
                    $dateTimeObjects['errors']['start'] = self::getI18NText('inputerror-startday-missing');

                if (!$dateTimeObjects['stop']->isValid())
                    $dateTimeObjects['errors']['stop'] = self::getI18NText('inputerror-stopday-missing');

                if (count($dateTimeObjects['errors']))
                    return $dateTimeObjects;

                if ($dateTimeObjects['start']->getUnixtime() > $dateTimeObjects['stop']->getUnixtime())
                {
                    $dateTimeObjects['start'] = false;
                    $dateTimeObjects['stop'] = false;
                    $dateTimeObjects['errors']['timeframe'] = self::getI18NText('inputerror-stopday-not-after-startday');
                    return $dateTimeObjects;
                }
            break;
            case self::MODE_MONTH_TO_MONTH:
                if (   $Request->timeframe_start_m == 'NULL'
                    && $Request->timeframe_start_y == 'NULL'
                    && $Request->timeframe_stop_m == 'NULL'
                    && $Request->timeframe_stop_y == 'NULL')
                {
                    $dateTimeObjects['start'] = $dateTimeObjects['stop'] = null;
                    return $dateTimeObjects;
                }

                // Start validieren
                if (!isset($Request->timeframe_start_m) || $Request->timeframe_start_m == 'NULL')
                    $dateTimeObjects['errors']['start'] = self::getI18NText('inputerror-startmonth-missing');

                if (!isset($Request->timeframe_start_y) || $Request->timeframe_start_y == 'NULL')
                    $dateTimeObjects['errors']['start'] = self::getI18NText('inputerror-startmonth-missing');

                if (!isset($dateTimeObjects['errors']['start']))
                    $dateTimeObjects['start'] = new BlibsDateTime(sprintf('%s-%s-01 00:00:00'
                                                                          , $Request->timeframe_start_y
                                                                          , str_pad($Request->timeframe_start_m, 2, '0', STR_PAD_LEFT)));

                // Stop validieren
                if (!isset($Request->timeframe_stop_m) || $Request->timeframe_stop_m == 'NULL')
                    $dateTimeObjects['errors']['stop'] = self::getI18NText('inputerror-stopmonth-missing');

                if (!isset($Request->timeframe_stop_y) || $Request->timeframe_stop_y == 'NULL')
                    $dateTimeObjects['errors']['stop'] = self::getI18NText('inputerror-stopmonth-missing');

                if (!isset($dateTimeObjects['errors']['stop']))
                {
                    $Helper = new BlibsDate();
                    $Helper->setDateParts($Request->timeframe_stop_y, $Request->timeframe_stop_m, 1);
                    $dateTimeObjects['stop'] = new BlibsDateTime(sprintf('%s-%s-%s 23:59:59'
                                                                         , $Request->timeframe_stop_y
                                                                         , str_pad($Request->timeframe_stop_m, 2, '0', STR_PAD_LEFT)
                                                                         , $Helper->getLastDayOfMonth()));
                }

                // Falls Fehler raus damit
                if (count($dateTimeObjects['errors']))
                    return $dateTimeObjects;

                if ($dateTimeObjects['start']->getUnixtime() > $dateTimeObjects['stop']->getUnixtime())
                {
                    $dateTimeObjects['start'] = false;
                    $dateTimeObjects['stop'] = false;
                    $dateTimeObjects['errors']['timeframe'] = self::getI18NText('inputerror-stopmonth-not-after-startmonth');
                    return $dateTimeObjects;
                }
            break;
            case self::MODE_YEAR_TO_YEAR:
                if ($Request->timeframe_start_y == 'NULL' && $Request->timeframe_stop_y == 'NULL')
                {
                    $dateTimeObjects['start'] = $dateTimeObjects['stop'] = null;
                    return $dateTimeObjects;
                }

                // Start validieren
                if (!isset($Request->timeframe_start_y) || $Request->timeframe_start_y == 'NULL')
                    $dateTimeObjects['errors']['start'] = self::getI18NText('inputerror-startyear-missing');

                if (!isset($dateTimeObjects['errors']['start']))
                    $dateTimeObjects['start'] = new BlibsDateTime(sprintf('%s-01-01 00:00:00'
                                                                          , $Request->timeframe_start_y));

                // Stop validieren
                if (!isset($Request->timeframe_stop_y) || $Request->timeframe_stop_y == 'NULL')
                    $dateTimeObjects['errors']['stop'] =  self::getI18NText('inputerror-stopyear-missing');

                if (!isset($dateTimeObjects['errors']['stop']))
                {
                    $Helper = new BlibsDate();
                    $Helper->setDateParts($Request->timeframe_stop_y, $Request->timeframe_stop_m, 1);
                    $dateTimeObjects['stop'] = new BlibsDateTime(sprintf('%s-12-31 23:59:59'
                                                                         , $Request->timeframe_stop_y));                }

                // Falls Fehler raus damit
                if (count($dateTimeObjects['errors']))
                    return $dateTimeObjects;

                if ($dateTimeObjects['start']->getUnixtime() > $dateTimeObjects['stop']->getUnixtime())
                {
                    $dateTimeObjects['start'] = false;
                    $dateTimeObjects['stop'] = false;
                    $dateTimeObjects['errors']['timeframe'] = self::getI18NText('inputerror-stopyear-not-after-startyear');
                    return $dateTimeObjects;
                }
            break;
            default:
                $dateTimeObjects['errors']['timeframe'] = self::getI18NText('inputerror-unknown-mode');
            break;
        }

        return $dateTimeObjects;
    }
    // End getDateTimeObjectsByRequest

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Schaut mal grob nach ob der Nutzer was sinnvolles in das Eingabefeld des Timeframe-Dings eingegeben hat.
     */
    protected static function validateDateInput($input)
    {
        $parts = explode('.', $input);
        if (count($parts) != 3)
            return false;

        foreach ($parts as $p)
            if (!is_numeric($p))
                return false;

        if ($parts[0] < 1 || $parts[0] > 31)
            return false;

        if ($parts[1] < 1 || $parts[1] > 12)
            return false;

        if ((int) $parts > 99 && (int) $parts < 999)
            return false;

        if ((int) $parts[2] < 99)
            $parts[2] = $parts[2] + 2000;

        return ['day' => (int) $parts[0], 'month' => (int) $parts[1], 'year' => (int) $parts[2]];
    }
    // End validateDateInput

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    protected static function getI18NText($key)
    {
        if (is_object(self::$I18nCatalog) && isset(self::$I18nCatalog->$key))
            return self::$I18nCatalog->$key;

        switch ($key)
        {
            case 'input-label-day':                           return 'Tag: '; break;
            case 'input-label-month':                         return 'Month: '; break;
            case 'input-label-year':                          return 'Jahr: '; break;
            case 'input-label-from':                          return 'From: '; break;
            case 'input-label-to':                            return 'To: '; break;
            case 'null-option-stop-year':                     return '-- Alle --'; break;
            case 'null-option-start-year':                    return '-- Alle --'; break;
            case 'null-option-start-month':                   return '-- Alle --'; break;
            case 'null-option-stop-month':                    return '-- Alle --'; break;
            case 'submit_link_caption_default':               return 'eingrenzen'; break;
            case 'submit_link_caption_choose':                return 'wählen'; break;

            case 'inputerror-invalid-date':                   return "Bitte geben Sie im Feld 'Tag' ein gültiges Datum ein"; break;
            case 'inputerror-month-missing':                  return "Bitte wählen Sie einen Monat"; break;
            case 'inputerror-startday-missing':               return "Bitte geben Sie im Feld 'Von' ein gültiges Datum ein"; break;
            case 'inputerror-stopday-missing':                return "Bitte geben Sie im Feld 'Bis' ein gültiges Datum ein"; break;
            case 'inputerror-stopday-not-after-startday':     return 'Bitte geben Sie ein Startdatum ein, welches vor dem Stopdatum liegt.'; break;
            case 'inputerror-startmonth-missing':             return 'Bitte wählen Sie einen Anfangsmonat'; break;
            case 'inputerror-stopmonth-missing':              return 'Bitte wählen Sie einen Endmonat'; break;
            case 'inputerror-stopmonth-not-after-startmonth': return 'Bitte wählen Sie einen Anfangsmonat, welcher vor dem Endmonat liegt.'; break;
            case 'inputerror-startyear-missing':              return 'Bitte wählen Sie ein Anfangsjahr'; break;
            case 'inputerror-stopyear-missing':               return 'Bitte wählen Sie ein Endjahr'; break;
            case 'inputerror-stopyear-not-after-startyear':   return "Bitte wählen Sie ein Startjahr, welches vor dem Endjahr liegt."; break;
            case 'inputerror-unknown-mode';                   return "Fehler bei der zeitlichen Eingrenzung: 'Unbekannter Auswahlmodus'"; break;
        }
    }
    // End getI18NText

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTimeframeHead
