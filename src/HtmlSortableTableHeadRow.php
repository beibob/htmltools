<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 * A html table head row with sort links
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlSortableTableHeadRow extends HtmlTableHeadRow
{
    private $sortParam;
    private $dirParam;
    private $sortColumns;
    private $attributes;
    private $currentSort;
    private $currentDir;

    private $TableProperties;

    /**
     * Base url without arguments to send arguments to
     */
    private $BaseUrl;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new html table head row
     *
     * @param string $sortParam    the parameter for the request (the sort column)
     * @param string $dirParam     the parameter for the request (the sort direction)
     * @param array $sortColumns   the columns to sort by
     */
    public function __construct(HtmlTableProperties $HtmlTableProperties, $baseUrl = null)
    {
        $this->TableProperties = $HtmlTableProperties;

        $this->BaseUrl = Url::parse($baseUrl);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the table columns
     *
     * @param  array $columns
     */
    public function setColumns(array $columns)
    {
        parent::setColumns($columns);

        foreach($this->getColumns() as $name => $column)
        {
            $column->appendAttribute('class', 'sortable-tablehead');
            $ODiv = $column->setOutputElement(new HtmlTag('div'));
            $ODiv->setAttribute('class', 'order-container');
            $Div = $ODiv->add(new HtmlTag('div'));
            if (StringFactory::unCamelCase($column->getName()) == $this->TableProperties->getOrderColumn())
            {
                $Url = clone $this->BaseUrl;
                $Url->addParameter(array('table' => $this->TableProperties->getIdent(),
                                         'sort'  => StringFactory::unCamelCase($column->getName()),
                                         'dir'   => ($this->TableProperties->getOrderDir() == 'ASC' ? 'DESC' : 'ASC')));

                $Link = $Div->add(new HtmlLink($column->getCaption(), (string)$Url));
                $Link->appendAttribute('class', 'caption active');

                $Link = $Div->add(new HtmlLink(' ', (string)$Url));
                $Link->appendAttribute('class', 'order-icon active ' . strtolower($this->TableProperties->getOrderDir()));
            }
            else
            {
                $Url = clone $this->BaseUrl;
                $Url->addParameter(array('table' => $this->TableProperties->getIdent(),
                                         'sort'  => StringFactory::unCamelCase($column->getName()),
                                         'dir'   => 'ASC'));

                $Link = $Div->add(new HtmlLink($column->getCaption(), (string)$Url));
                $Link->appendAttribute('class', 'caption');

                $Link = $Div->add(new HtmlLink(' ', (string)$Url));
                $Link->appendAttribute('class', 'order-icon');
            }
        }
    }
    // End setColumns

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableRow
