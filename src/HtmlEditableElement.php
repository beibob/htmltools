<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\DbObject;
use Beibob\HtmlTools\Interfaces\Converter;

/**
 * Implements an editable HTML element.
 *
 * An editable element is categorized by a value
 * and the possibility to switch the readonly property
 * on.
 *
 * @package blibs
 * @author     Thorsten M�rell <thorsten.muerell@beibob.net>
 *
 */
abstract class HtmlEditableElement extends HtmlDataElement
{
    /**
     * The value of the element
     */
    private $value;

    /**
     * ReadOnly-Flag
     */
    private $readonly;

    /**
     * @var bool
     */
    protected $disabled;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new editable element
     *
     * @param  string $name                   the name of the element (also known as property)
     * @param  string $value                  the value of the element
     * @param  boolean $readonly              whether the element is readonly
     * @param  Converter $DefaultConverter    the default converter
     * @param  DbObject $DataObject           the data object to use
     */
    public function __construct($name, $value = null, $readonly = false, Converter $DefaultConverter = null, DbObject $DataObject = null)
    {
        parent::__construct($name, $DefaultConverter, $DataObject);

        if(!is_null($value))
            $this->setValue($value);

        $this->setReadonly($readonly, $readonly);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the value of the element
     *
     * @param  mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
    // End setValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the readonly-flag
     *
     * @param  bool $readonly
     */
    public function setReadonly($readonly = true, $disabled = true)
    {
        $this->readonly = $readonly;
        $this->disabled = $disabled;

        if($readonly || $disabled)
            $this->addClass('read-only');
        else
            $this->removeClass('read-only');
    }
    // End setReadonly

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value of the element
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
    // End getValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the state of readonly-Flag
     *
     * @return bool
     */
    public function isReadonly()
    {
        return $this->readonly;
    }
    // End isReadonly


    /**
     * Returns the state of disabled-Flag
     *
     * @return bool
     */
    public function isDisabled()
    {
        return $this->disabled;
    }
    // End isReadonly
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the raw text value after calling the converter chain
     *
     * @return string    the text value
     */
    public function getConvertedTextValue($value = null)
    {
        return parent::getConvertedTextValue(!is_null($value)? $value : $this->getValue());
    }
    // End getConvertedTextValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value of the element
     *
     * @return mixed
     */
    public function getValidatedFormDataObject()
    {
        $value = $this->getValue();
        return $this->getValidatedFormDataObjectForValue($value);
    }
    // End getValidatedFormDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the readonly flag on all children that are HtmlEditableElements
     *
     * @param  HtmlElement $Element   The html element to start with
     * @param  boolean $readOnly
     */
    public static function setReadonlyRecursive(HtmlElement $Element, $readonly = true, $disabled = true)
    {
        foreach($Element->getChildren() as $Child)
        {
            if($Child instanceof HtmlEditableElement) {
                $Child->setReadonly($readonly, $disabled);
            }

            self::setReadonlyRecursive($Child, $readonly, $disabled);
        }
    }
    // End setReadonlyRecursive

    //////////////////////////////////////////////////////////////////////////////////////


}
// End HtmlEditableElement
