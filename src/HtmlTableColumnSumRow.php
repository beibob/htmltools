<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 * Sum of a table row
 *
 * @package blibs
 * @author Dakam Wandji <franck@beibob.de>
 *
 */
class HtmlTableColumnSumRow  extends HtmlTableRow
{
    // Name of the cullomn to make the sum
    private $sumColumn;

    // Total Sum switch True or false
    private $totalSumSwitch = true;

    // Displayed Sum switch True or false
    private $displayedSumSwitch = true;

    // Total Sum Value
    private $totalSum ;

    // Displayed Sum  Value
    private $displayedSum ;

    private $totalSumCaption;

    private $displayedSumCaption ;

    /**
     * Creates a new html table row group
     *
     * @param  HtmlTableProperties $Properties       the html table row
     * 
     */
    public function __construct($sumColumn, HtmlTableProperties $TableProperties = null , $sum = NULL , $displayedSum = NULL)
    {
        if (!is_null($TableProperties))
            $this->setTableProperties($TableProperties);
        
        if (!is_null($sum))
            $this->setTotalSum($sum);

        if (!is_null($displayedSum))
            $this->setDisplayedSum( $displayedSum );

        $this->setSumColumn($sumColumn);

    }        
    // End __construct

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Set Table Property
     * 
     * @param <type> $Properties
     */
    public function setTableProperties($TableProperties)
    {
        $this->TableProperties = $TableProperties;
    }
    //End setTableProperties

    /**
     * Set sumColumn
     *
     * @param <type> $sumColumn
     *
     */
    public function setSumColumn($sumColumn)
    {
        $this->sumColumn = $sumColumn;
    }
    //End setSumColumn

    /**
     * Get sumColumn
     *
     * @param <type> $sumColumn
     * @return $sumColumn
     */
    public function getSumColumn()
    {
        return $this->sumColumn;
    }
    //End getSumColumn

    /**
     * Enable Total Sum
     *
     * @return totalSumSwitch
     */
    public function enableTotalSum($state = true)
    {
        return $this->totalSumSwitch = $state;
    }
    //End enableTotalSum

    /**
     * Enable Displayed Sum
     *
     * @return displayedSumSwitch
     */
    public function enableDisplayedSum($state = true)
    {
        return $this->displayedSumSwitch = $state;
    }
    //End enableDisplayedSum

    /**
     * Set Total Sum Caption
     *
     * @param <type> $text
     *
     */
    public function setTotalSumCaption($text)
    {
      $this->totalSumCaption = $text;
    }
    //End setTotalSumCaption

    /**
     * Set Displayed Sum Caption
     *
     */
    public function setDisplayedSumCaption($text)
    {
        $this->displayedSumCaption = $text;

    }
    //End setDisplayedSumCaption

    /**
     * get Total Sum Caption
     *
     * @param <type> $text
     * @return totalSumCaption
     */
    public function getTotalSumCaption()
    {
      return $this->totalSumCaption;
    }
    //End getTotalSumCaption

    /**
     * Set Displayed Sum Caption
     * @return displayedSumCaption
     */
    public function getDisplayedSumCaption()
    {
        return $this->displayedSumCaption;
    }
    //End setDisplayedSumCaption

    /**
     * get Total Sum Caption
     *
     * @param <type> $text
     * @return totalSumCaption
     */
    public function getTotalSum()
    {
      return $this->totalSum;
    }
    //End getTotalSum

    /**
     * Set Displayed Sum Caption
     * @return displayedSumCaption
     */
    public function getDisplayedSum()
    {
        return $this->displayedSum;
    }
    //End setDisplayedSum

    /**
     * get Total Sum Caption
     *
     * @param <type> $text
     * @return totalSumCaption
     */
    public function setTotalSum( $sum )
    {
      $this->totalSum = $sum;
    }
    //End setTotalSum

    /**
     * Set Displayed Sum Caption
     * @return displayedSumCaption
     */
    public function setDisplayedSum( $sum )
    {
        $this->displayedSum = $sum;
    }
    //End setDisplayedSum


    ////////////////////////////////////////////////////////////////////////////
    /**
     * Sets the table columns
     *
     * @param  array $columns
     */
    public function setColumns(array $columns)
    {
        parent::setColumns($columns);
        
        foreach($this->getColumns() as $name => $column)
        { 
            $column->appendAttribute('colspan', count($this->getColumns()));


            $DisplayedSumDiv = new HtmlTag('div',$this->getDisplayedSum());
            $DisplayedSumDiv->setAttribute('class', 'displayedDivValue');
            $DisplayedSumCaptionDiv = new HtmlTag('div',$this->getDisplayedSumCaption());
            $DisplayedSumCaptionDiv->setAttribute('class', 'displayedDivCaption');

            $TotalSumDiv = new HtmlTag('div',$this->getTotalSum());
            $TotalSumDiv->setAttribute('class', 'totalDivValue');
            $TotalSumCaptionDiv = new HtmlTag('div',$this->getTotalSumCaption());
            $TotalSumCaptionDiv->setAttribute('class', 'totalDivCaption');



            $Div = new HtmlTag('div');
            $Div->add($DisplayedSumCaptionDiv);
            $Div->add($DisplayedSumDiv);
            $Div->add($TotalSumCaptionDiv);
            $Div->add($TotalSumDiv);
            $Div->setAttribute('class', 'TableSum');

            $Clearing = $Div->add(  new HtmlTag('div'));
            $Clearing->setAttribute('class', 'clearing');

            $Div = $column->setOutputElement($Div);


            return;
        }
    }
    // End setColumns

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableColumnSumRow
