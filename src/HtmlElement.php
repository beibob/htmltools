<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\IdFactory;
use Beibob\HtmlTools\Interfaces\Formatter;
use DOMDocument;
use DOMNode;

/**
 * HTML element base class. This class is the base class for all
 * HTML elements. All HTML elements have a set of attributes, a set
 * of attribute formatters and a set of childen.
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten M�rell <thorsten.muerell@beibob.net>
 * @abstract
 *
 */
abstract class HtmlElement
{
    /**
     * Attributes
     */
    private $attributes = [];

    /**
     * These are the children of this element
     */
    private $children = [];

    /**
     * These are the registered formatters
     */
    private $attrFormatters = [];

    /**
     * Parent element
     */
    private $parent;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds the element
     *
     * @param  DOMDocument $DOMDocument   the DOM document to build the element with
     * @return DOMNode   the node with the content of the the element
     */
    abstract public function build(DOMDocument $DOMDocument);
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Appends the element to the given node
     *
     * @param  DOMNode $DOMNode
     * @return DOMNode
     */
    public function appendTo(DOMNode $DOMNode)
    {
        $DOMDocument = $DOMNode instanceOf DOMDocument? $DOMNode : $DOMNode->ownerDocument;

        if(!$HtmlElt = $this->build($DOMDocument))
            return $DOMNode;

        return $DOMNode->appendChild($HtmlElt);
    }
    // End appendTo

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a child to the list of children
     *
     * @param  HtmlElement $Element   The child to add
     * @return HtmlElement   The element just added
     */
    public function addChild(HtmlElement $Element) {
        $Element->parent = $this;
        return $this->children[] = $Element;
    }
    // End addChild

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a child to the list of children. Alias for the method addChild.
     *
     * @param  HtmlElement $Element   The child to add
     * @return HtmlElement   The element just added
     */
    final public function add(HtmlElement $Element) {
        return $this->addChild($Element);
    }
    // End add

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the list of children
     *
     * @return array(HTMLElement)   the children of this element
     */
    public function getChildren() {
        return $this->children;
    }
    // End getChildren

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the parent
     *
     * @return HTMLElement
     */
    public function getParent() {
        return $this->parent;
    }
    // End getParent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Clears all the children
     */
    public function clearChildren() {
        $this->children = [];
    }
    // End clearChildren

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds an attribute formatter
     *
     * @param  Formatter $Formatter   the formatter to add to the jain
     * @return Formatter              the formatter just added
     */
    public function addAttrFormatter(Formatter $Formatter)
    {
        return $this->attrFormatters[] = $Formatter;
    }
    // End addAttrFormatter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets all the attribute formatters
     *
     * @return array      the list of formatters in their order
     */
    public function getAttrFormatters()
    {
        return $this->attrFormatters;
    }
    // End getAttrFormatters

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the id
     *
     * @param int
     * @return int
     */
    public function setId($id)
    {
        return $this->setAttribute('id', $id);
    }
    // End setId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the id and creates a unique one if necessary
     *
     * @param  bool $createUniqueIdIfNecessary
     * @return int
     */
    public function getId($createUniqueIdIfNecessary = false)
    {
        if($createUniqueIdIfNecessary && !$this->hasAttribute('id'))
            $this->setId(chr(rand(97, 122)) . IdFactory::getUniqueId());

        return $this->getAttribute('id');
    }
    // End getId

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets an attribute
     *
     * @param  string $name
     * @param  mixed  $value
     * @return mixed   the value just set
     */
    public function setAttribute($name, $value)
    {
        return $this->attributes[$name] = $value;
    }
    // End setAttribute

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Unsets an attribute
     *
     * @param  string $name
     * @return
     */
    public function removeAttribute($name)
    {
        unset($this->attributes[$name]);
    }
    // End removeAttribute

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Appends a value of an attribute
     *
     * @param  string $appendix
     * @param  mixed  $value
     * @return -
     */
    public function appendAttribute($name, $appendix)
    {
        if(!isset($this->attributes[$name]))
            return $this->setAttribute($name, $appendix);

        return $this->attributes[$name] = trim($this->attributes[$name] .' '. $appendix);
    }
    // End appendAttribute

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a specific string from the value of an attribute
     *
     * @param  string $appendix
     * @param  mixed  $value
     * @return -
     */
    public function cutAttribute($name, $appendix)
    {
        if(!isset($this->attributes[$name]))
            return;

        $parts = array_flip(preg_split('/\s/u', $this->attributes[$name], -1, PREG_SPLIT_NO_EMPTY));

        if(isset($parts[$appendix]))
            unset($parts[$appendix]);

        $this->attributes[$name] = join(' ', array_flip($parts));
    }
    // End cutAttribute

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns an attribute value
     *
     * @param  string $name
     * @return mixed
     */
    public function getAttribute($name)
    {
        return isset($this->attributes[$name])? $this->attributes[$name] : null;
    }
    // End getAttribute

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if an attribute value isset
     *
     * @param  string $name
     * @return boolean
     */
    public function hasAttribute($name)
    {
        return isset($this->attributes[$name]);
    }
    // End hasAttribute

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns all attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
    // End getAttributes

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if the class attribute has a value
     *
     * @param  string $cssClass
     * @return  boolean
     */
    public function hasClass($cssClass)
    {
        if(!isset($this->attributes['class']))
            return false;

        return (bool)preg_match('/(^|\s)'. preg_quote($cssClass) .'(\s|$)/u', $this->attributes['class']);
    }
    // End hasClass

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a class to the class attribute
     *
     * @param  string $cssClass
     * @return mixed   the value just set
     */
    public function addClass($cssClass)
    {
        if($this->hasClass($cssClass))
            return $cssClass;

        return $this->appendAttribute('class', $cssClass);
    }
    // End addClass

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Removes a class from the class attribute
     *
     * @param  string $name
     * @return
     */
    public function removeClass($cssClass)
    {
        return $this->cutAttribute('class', $cssClass);
    }
    // End removeClass

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds all the attributes and adds the to the given DOM node
     *
     * @param  DOMNode $Elt            the \DOM node to add the attributes to
     * @param  \stdClass $DataObject    the data object to pass to the formatters
     * @param  mixed $property         the property to give to the formatters
     */
    public function buildAndSetAttributes(DOMNode $Elt, $DataObject = null, $property = null)
    {
        // Collect the formatter attributes
        $attributes = [];
        foreach ($this->getAttrFormatters() as $Formatter) {
            $output = $Formatter->format($this, $DataObject, $property);
            if (is_array($output))
                $attributes = array_merge($attributes, $output);
        }

        // Merge in the object attributes
        $attributes = array_merge($this->getAttributes(), $attributes);

        foreach ($attributes as $attribute => $value) {
            if(!is_null($value))
                $Elt->setAttribute($attribute, $value);
        }
    }
    // End buildAndSetAttributes

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlElement
