<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Baut eine Zeile in der rechts die aktuelle Anzahl der Datensätze steht
 * und links ein Navielement zum durchblättern erscheint
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlTableFilterRow extends HtmlTableHeadRow
{
    private $TableProperties;

    /**
     * Creates a new html table head row
     *
     * @param string $sortParam    the parameter for the request (the sort column)
     * @param string $dirParam     the parameter for the request (the sort direction)
     * @param array $sortColumns   the columns to sort by
     */
    public function __construct(HtmlTableProperties $Properties)
    {
        $this->TableProperties = $Properties;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds a single table row
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $DOMDocument)
    {
        if (!$this->TableProperties->getFilterState())
            return;

        return parent::build($DOMDocument);
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the table columns
     *
     * @param  array $columns
     */
    public function setColumns(array $columns)
    {
        parent::setColumns($columns);
        $this->setDataObject(null);

        $columns = $this->getColumns();
        $keys = array_keys($columns);
        $FirstColumn = $columns[$keys[0]];

        $added = [];
        $hiddens = [];
        foreach ($this->TableProperties->getFilters() as $Filter)
        {
            if (isset($columns[$Filter->getFilterProperty()]))
            {
                $Column = $columns[$Filter->getFilterProperty()];
                $ODiv = $Column->setOutputElement(new HtmlTag('div'));
                $ODiv->setAttribute('class', 'filter');
                $Filter->appendTo($ODiv);
            }
            else
                $hiddens[] = $Filter;

            $added[$Filter->getFilterProperty()] = true;
        }
        foreach($hiddens as $Filter)
            $Filter->appendTo($FirstColumn);

        foreach ($columns as $property => $Column)
        {
            $Column->appendAttribute('class', 'filter-tablehead');

            if (isset($added[$property]) || $FirstColumn === $Column)
                continue;

            $Div = $Column->setOutputElement(new HtmlTag('div'));
            $Div->setAttribute('class', 'clearing');
        }
    }
    // End setColumns

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableRow
