<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * An element which creates a HTML link.
 *
 * The attribute href of the element is set to the value of the
 * getConvertedTextValue call of HtmlDataElement.
 *
 * The content is build from the children.
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlLink extends HtmlElement
{
    /**
     * The link target
     */
    private $caption;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new static text element
     *
     * @param string $value the text to display
     */
    public function __construct($text, $href = '#', $target = null)
    {
        $this->caption = $text;
        $this->setAttribute('href', $href);
        $this->setAttribute('target', $target);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element.
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $A = $Document->createElement('a');
        $A->appendChild($Document->createTextnode($this->caption));

        $this->buildAndSetAttributes($A);

        foreach ($this->getChildren() as $Child)
            $Child->appendTo($A);

        return $A;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlLink
