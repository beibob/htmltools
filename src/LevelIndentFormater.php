<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 * Formater adding a class to control the ident of an table cell by an level property
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class LevelIndentFormater extends AbstractFormatter
{
    /**
     * used property
     */
    private $property;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Option to overwrite the used property
     */
    public function __construct($property = null)
    {
        $this->property = $property;
    }
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Formats the object
     *
     * @see Formatter::format()
     */
    public function format(HtmlElement $obj, $DataObject = null, $property = null)
    {
        $usedProperty = is_null($this->property) ? $property : $this->property;
        $obj->setAttribute('class', 'level-' . $DataObject->$usedProperty);
    }
    // End format

    //////////////////////////////////////////////////////////////////////////////////////
}
// End LevelIndentFormater
