<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Implements a grouping of radioboxes
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlRadioGroup extends HtmlFormElement
{
    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $GroupElt = $Document->createElement('div');
        $GroupElt->setAttribute('class', 'group-container clearfix');

        foreach ($this->getChildren() as $Child) {
            $Child->appendTo($GroupElt);
        }

        /**
         * Set remaining attributes
         */
        $this->buildAndSetAttributes($GroupElt, $this->getDataObject(), $this->getName());

        return $GroupElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a child to this RadioGroup.
     *
     * Note: Only children of type HtmlRadiobox can be added.
     *
     * @param  HtmlElement $Element the element to add to this radio group
     * @return HtmlElement the added element
     */
    public function addChild(HtmlElement $Element)
    {
        if ($Element instanceof HtmlRadiobox)
            $Element->setRadioGroup($this);

        return parent::addChild($Element);
    }
    // End addChild

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlRadioGroup
