<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Implements a HTML multi select box container.
 *
 * Usage:
 *
 * <code>
 *   $SelectBox = $Form->addChild(new HtmlFormElementLabel('Label', new HtmlMultiSelectBox('property')));
 *   $SelectBox->add(new HtmlSelectOption('name', 'value'));
 *   $SelectBox->add(new HtmlSelectOption('name2', 'value2'));
 * </code>
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class HtmlMultiSelectbox extends HtmlSelectbox
{
    /**
     * Builds this element.
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $this->setAttribute('multiple', 'multiple');
        return parent::build($Document);
    }
    // End build
}
// End HtmlMultiSelectbox