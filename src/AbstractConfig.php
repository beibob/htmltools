<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\HtmlTools\Interfaces\Configurable;

/**
 * Abstract implementation of the Configurable interface
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 * @abstract
 *
 */
abstract class AbstractConfig implements Configurable
{
    /**
     * Default config parameters
     */
    protected $defaultParameters = [];

    /**
     * Config parameters
     */
    private $parameters = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets a config value
     *
     * @param string $name
     * @param mixed  $value
     * @return AbstractConverter
     */
    public function set($name, $value)
    {
        $this->parameters[$name] = $value;
        return $this;
    }
    // End set

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the configured value or the defaultValue
     *
     * @param  string $name
     * @param  mixed  $defaultValue [optional]
     * @return mixed
     */
    public function get($name, $defaultValue = null)
    {
        if(!isset($this->parameters[$name]))
        {
            if(isset($this->defaultParameters[$name]))
                return $this->defaultParameters[$name];

            return $defaultValue;
        }

        return $this->parameters[$name];
    }
    // End get

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if a config value exists
     *
     * @param string $name
     * @return boolean
     */
    public function has($name)
    {
        return !is_null($this->get($name));
    }
    // End has

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns all configured parameters
     *
     * @return array
     */
    public function getConfig()
    {
        return array_merge($this->defaultParameters, $this->parameters);
    }
    // End getConfig
}
// End AbstractConfig
