<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * This class implements a simple textual description in formcontext
 *
 * The generated HTML looks something like that:
 *
 * <code>
 * </code>
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class HtmlFormElementDescription extends HtmlElement
{
    private $innerTag = 'p';

    /**
     * The description-text
     */
    private $text;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new element
     *
     * @param string $label the label text for the element
     */
    public function __construct($text)
    {
        $this->text = $text;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getText()
    {
        return $this->text;
    }
    // End getText

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setInnerTag($tag)
    {
        $this->innerTag = $tag;
    }
    // End setInnerTag

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $FirstFormElement = HtmlFormElement::searchFirstFormElementChild($this);

        $DivElt = $Document->createElement('div');

        $this->buildAndSetAttributes($DivElt);
        $DivElt->setAttribute('class', $DivElt->getAttribute('class') . ' descriptionElement');

        $DescriptionDiv = $Document->createElement('div');
        $DescriptionDiv->setAttribute('class', 'description');

        $P = $Document->createElement($this->innerTag);

        $P->appendChild($Document->createTextnode($this->text));
        $DescriptionDiv->appendChild($P);

        $DivElt->appendChild($DescriptionDiv);

        return $DivElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlFormElementDescription
