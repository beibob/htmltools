<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 * Baut eine Zeile in der rechts die aktuelle Anzahl der Datensätze steht
 * und links ein Navielement zum durchblättern erscheint
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlTablePagerRow extends HtmlTableRow
{
    const DEFAULT_PAGE_DISPLAY_COUNT = 9;
    private $pageDisplayCount = self::DEFAULT_PAGE_DISPLAY_COUNT;
    private $TableProperties;
    private $requestMethod = 'GET';
    private $activeDecorationPrefix = false;
    private $activeDecorationSuffix = false;
    private $noDataFoundText = 'No data found';
    
    /**
     * Creates a new html table head row
     *
     * @param string $sortParam    the parameter for the request (the sort column)
     * @param string $dirParam     the parameter for the request (the sort direction)
     * @param array $sortColumns   the columns to sort by
     */
    public function __construct(HtmlTableProperties $Properties, $requestMethod = 'GET', $activeDecorationPrefix = false, $activeDecorationSuffix = false, $noDataFoundText = 'No data found')
    {
        $this->TableProperties = $Properties;
        $this->setRequestMethod($requestMethod);
        $this->setActiveDecoration($activeDecorationPrefix, $activeDecorationSuffix);
        $this->setNoDataFoundText($noDataFoundText);
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the page index maximum
     *
     * @param  integer $displayCount
     * @return integer
     */
    public function setPageDisplayCount($displayCount = self::DEFAULT_PAGE_DISPLAY_COUNT)
    {
        return $this->pageDisplayCount = $displayCount;
    }
    // End setPageDisplayCount

    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * Sets the used HTTP Request method (GET or POST)
     * Not public! Can only called at initialisation time in constructor!
     *
     * @param string $method
     */
    protected function setRequestMethod($method = 'GET')
    {
        if ($method == 'GET' || $method == 'POST')
            $this->requestMethod = $method;

        return $this->requestMethod;
    }
    // End setRequestMethod

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the href or the onclick attribute
     *
     * @param HtmlLink $Link - Link to configure
     * @param integer $page - PageIndex
     */
    protected function configurePagerLink($Link, $page)
    {
        if ($this->requestMethod == 'GET')
            $Link->setAttribute('href', '?table=' . $this->TableProperties->getIdent() . '&page=' . $page);
        else
            $Link->setAttribute('onclick', sprintf("blibsForm.setActionAndSubmitValue(event, '%s', %s);"
                                                    , FrontController::getInstance()->getLinkTo()
                                                    , StringFactory::buildJsLiteralNotation(array('refresh' => 0, 'table' => $this->TableProperties->getIdent(), 'page' => $page))
                                                  ));
    }
    // End configurePagerLink

    //////////////////////////////////////////////////////////////////////////////////////
    
    /**
     *
     * @param string $prefix
     * @param string $suffix
     */
    protected function setActiveDecoration($prefix = false, $suffix = false)
    {
        $this->activeDecorationPrefix = $prefix;
        $this->activeDecorationSuffix = $suffix;
    }
    // End setActiveDecoration
    
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param string $text
     * @param string $suffix
     */
    protected function setNoDataFoundText($text = 'No data found')
    {
        $this->noDataFoundText = $text;
    }
    // End setNoDataFoundText

    //////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Sets the table columns
     *
     * @param  array $columns
     */
    public function setColumns(array $columns)
    {
        parent::setColumns($columns);

        foreach($this->getColumns() as $name => $column)
        {
            $column->appendAttribute('colspan', count($this->getColumns()));
            $ODiv = $column->setOutputElement(new HtmlTag('div'));
            $ODiv->setAttribute('class', 'pager-row');

            if ($this->TableProperties->getRecordCount())
            {
                $LeftDiv = $ODiv->add(new HtmlTag('div'));
                $LeftDiv->setAttribute('class', 'page-nav');

                if ($this->TableProperties->getPageCount() > 1)
                {
                    $Ul = $LeftDiv->add(new HtmlTag('ul'));
                    $First = $Ul->add(new HtmlTag('li'));
                    $First->setAttribute('class', 'first');
                    $Link = $First->add(new HtmlLink('<<', '#'));
                    $this->configurePagerLink($Link, 0);

                    $Prev = $Ul->add(new HtmlTag('li'));
                    $Prev->setAttribute('class', 'previous');
                    $prevIndex = $this->TableProperties->getPageIndex() > 0 ? $this->TableProperties->getPageIndex() - 1 : 0;
                    $Link = $Prev->add(new HtmlLink('<', '#'));
                    $this->configurePagerLink($Link, $prevIndex);

                    if ($this->TableProperties->getPageCount() > $this->pageDisplayCount)
                    {
                        $start = $this->TableProperties->getPageIndex() - floor($this->pageDisplayCount/2);
                        $start = ($start < 0) ? 0 : $start;

                        $stop = $start + $this->pageDisplayCount;
                        if ($stop > $this->TableProperties->getPageCount())
                            $stop = $this->TableProperties->getPageCount();
                    }
                    else
                    {
                        $start = 0;
                        $stop = $this->TableProperties->getPageCount();
                    }

                    for ($page = $start; $page < $stop; $page++)
                    {
                        $Li = $Ul->add(new HtmlTag('li'));
                        $caption = $page + 1;
                        if ($page == $this->TableProperties->getPageIndex())
                        {
                            $Li->setAttribute('class', 'active');
                            if ($this->activeDecorationPrefix)
                                $caption = $this->activeDecorationPrefix . $caption;
                            if ($this->activeDecorationSuffix)
                                $caption = $caption . $this->activeDecorationSuffix;
                        }
                        $Link = $Li->add(new HtmlLink($caption, '#'));
                        $this->configurePagerLink($Link, $page);
                    }

                    $nextIndex = $this->TableProperties->getPageIndex() < $this->TableProperties->getPageCount() ? $this->TableProperties->getPageIndex() + 1 : $this->TableProperties->getPageCount();
                    $Next = $Ul->add(new HtmlTag('li'));
                    $Next->setAttribute('class', 'next');
                    $Link = $Next->add(new HtmlLink('>', '#'));
                    $this->configurePagerLink($Link, $nextIndex);

                    $Last = $Ul->add(new HtmlTag('li'));
                    $Last->setAttribute('class', 'last');
                    $Link = $Last->add(new HtmlLink('>>', '#'));
                    $this->configurePagerLink($Link, $this->TableProperties->getPageCount() - 1);
                }

                $RightDiv = $ODiv->add(new HtmlTag('div'));
                $RightDiv->setAttribute('class', 'record-info');
                $Strong = $RightDiv->add(new HtmlTag('strong'));
                $Strong->add(new HtmlStaticText($this->TableProperties->getRecordCount()));

                $s = $this->TableProperties->getRecordCount() > 1 || $this->TableProperties->getRecordCount() == 0 ? 's' : '';
                $RightDiv->add(new HtmlStaticText(' Record' . $s . ' on '));
                $Strong = $RightDiv->add(new HtmlTag('strong'));
                $Strong->add(new HtmlStaticText($this->TableProperties->getPageCount()));
                $s = $this->TableProperties->getPageCount() > 1 || $this->TableProperties->getPageCount() == 0 ? 's' : '';
                $RightDiv->add(new HtmlStaticText(' Page' . $s));
            }
            else
            {
                $NoDataDiv = $ODiv->add(new HtmlTag('div'));
                $NoDataDiv->setAttribute('class', 'no-data');
                $Strong = $NoDataDiv->add(new HtmlTag('strong'));
                $Strong->add(new HtmlStaticText($this->noDataFoundText));
            }

            return;
        }
    }
    // End setColumns

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableRow
