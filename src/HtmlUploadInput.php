<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\DbObject;
use Beibob\HtmlTools\Interfaces\Converter;
use DOMDocument;

/**
 * Upload input form element
 *
 * @package htmltools
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlUploadInput extends HtmlInputElement
{
    /**
     * Creates a upload input element
     *
     * @see HtmlFormElement::__construct()
     */
    public function __construct($name, $value = null, $readonly = false, Converter $DefaultConverter = null, DbObject $DataObject = null, $Filetypes = null)
    {
        parent::__construct($name, $value, $readonly, $DefaultConverter, $DataObject, $Filetypes);
        $this->setType('file');
		$this->setAccept($Filetypes);
    }

    /**
     * Overwrite which sets the multipart/form-data attribute to the form element
     *
     * @see HtmlFormElement::setForm
     *
     * @param  HtmlForm $Form
     */
    public function setForm(HtmlForm $Form)
    {
        parent::setForm($Form);

        if ($Form->getAttribute('enctype') != 'multipart/form-data')
            $Form->setAttribute('enctype', 'multipart/form-data');
    }

    /**
     * Sets the accepted file types (comma separated list)
     */
    public function setAccept($filetypes)
    {
        return $this->setAttribute("accept", $filetypes);
    }
}
// End HtmlUploadInput
