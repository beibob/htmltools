<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\DbObject;
use Beibob\Blibs\XmlDocument;
use Beibob\HtmlTools\Interfaces\Converter;
use DOMDocument;
use DOMNode;
use DOMNodelist;

/**
 * Another magic element! It can import \DOMStructures
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class HtmlDOMImportElement extends HtmlDataElement
{
    /**
     * Stuff to import can be some of \DOMNode, \DOMNodelist, \DOMDocument or XMLDocument
     * @Todo: Would be nice to import instances of Controllers
     */
    private $ImportContent;
    private $throwException = true;
    private $errorMessage = '';

    const DEFAULT_ERROR_MESSAGE = 'Could only import instances of \DOMNode, \DOMNodelist \DOMDocument and Beibob\Blibs\XmlDocument.';
    
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the new element
     *
     * @param mixed $ImportContent - Instance of \DOMNode, \DOMNodelist, \DOMDocument or XmlDocument
     */
    public function __construct($ImportContent = null, $name = null, Converter $defaultConverter = null, DbObject $DataObject = null, $throwException = true, $errorMessage = '')
    {
        if (!is_null($ImportContent))
            $this->setImportContent($ImportContent);
        $this->throwException = $throwException;
        $this->errorMessage = $errorMessage;
        if ($this->errorMessage == '')
            $this->errorMessage = self::DEFAULT_ERROR_MESSAGE;
            
        if (!is_null($name))
            parent::__construct($name, $defaultConverter, $DataObject);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * sets the structure to import
     *
     * @param mixed $ImportContent - Instance of \DOMNode, \DOMDocument or XmlDocument
     */
    public function setImportContent($ImportContent)
    {
        if (   $ImportContent instanceof XmlDocument
            || $ImportContent instanceof DOMDocument
            || $ImportContent instanceof DOMNode
            || $ImportContent instanceof DOMNodelist)
            return $this->ImportContent = $ImportContent;

        throw new \Exception(self::DEFAULT_ERROR_MESSAGE);
    }
    // End setImportContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the dom structure
     */
    public function getImportContent()
    {
        return $this->ImportContent;
    }
    // End getImportContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the dom structure
     */
    public function unsetImportContent()
    {
        return $this->ImportContent = null;
    }
    // End getImportContent

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element.
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        if ($this->ImportContent instanceof DOMDocument)
        {
            if ($this->ImportContent instanceof XmlDocument)
                $ImportNode = $this->ImportContent->getContentNode();
            else
                $ImportNode = $this->ImportContent->firstChild;

            if ($ImportNode instanceof DOMNode)
                $Elt = $Document->importNode($ImportNode, true);
            else
                $Elt = $Document->createTextNode();

            return $Elt;
        }
        elseif ($this->ImportContent instanceof DOMNodelist)
        {
            $Elt = $Document->createElement('div');
            foreach ($this->ImportContent as $Node)
                $Elt->appendChild($Document->importNode($Node, true));

            return $Elt;
        }
        elseif ($this->ImportContent instanceof DOMNode)
        {
            $Elt = $Document->importNode($this->ImportContent, true);
            return $Elt;
        }
        else
        {
            $value = $this->getConvertedTextValue();
            if ($value)
            {
                $ImportDocument = new XmlDocument();
                if (!@$ImportDocument->loadXML($value)) {
                    if (!@$ImportDocument->loadXML("<div>" . $value . "</div>")) {
                        if ($this->throwException)
                            throw new Exception($this->errorMessage);
                        else
                            return $Document->createTextNode($this->errorMessage);
                    }
                    else
                    {
                        $this->setImportContent($ImportDocument);
                        $Elt = $this->build($Document);
                        $this->unsetImportContent();
                        return $Elt;
                    }
                }
                else
                {
                    $this->setImportContent($ImportDocument);
                    $Elt = $this->build($Document);
                    $this->unsetImportContent();
                    return $Elt;
                }
            }
            else
            {
                if ($this->throwException)
                    throw new Exception($this->errorMessage);
                else
                    return $Document->createTextNode($this->errorMessage);
            }
        }
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End Html\DOMImportElement
