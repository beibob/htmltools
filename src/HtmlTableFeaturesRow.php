<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 * Baut eine Zeile in der rechts die aktuelle Anzahl der Datensätze steht
 * und links ein Navielement zum durchblättern erscheint
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlTableFeaturesRow extends HtmlTableHeadRow
{
    private $TableProperties;
    private $features = array();

    private $limits = array(10, 20, 50);

    const FEATURE_EXPORT = 'export';
    const FEATURE_FILTER = 'filter';
    const FEATURE_LIMITER = 'limiter';
    const FEATURE_TIMEFRAME = 'timeframe';
    const FEATURE_ADDICON = 'add-icon';

    private $requestMethod = 'GET';

    /**
     * Creates a new html table head row
     *
     * @param string $sortParam    the parameter for the request (the sort column)
     * @param string $dirParam     the parameter for the request (the sort direction)
     * @param array $sortColumns   the columns to sort by
     */
    public function __construct(HtmlTableProperties $Properties, $requestMethod = 'GET')
    {
        $this->TableProperties = $Properties;
        
        if ($requestMethod == 'GET' || $requestMethod == 'POST')
            $this->requestMethod = $requestMethod;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setLimits(array $limits = array(10, 20, 50))
    {
        $this->limits = $limits;
    }
    // End setLimits

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function enableFeature($ident)
    {
        $this->features[$ident] = true;
    }
    // End enableFeature

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the table columns
     *
     * @param  array $columns
     */
    public function setColumns(array $columns)
    {
        parent::setColumns($columns);

        foreach($this->getColumns() as $name => $Column)
        {
            $Column->appendAttribute('colspan', count($this->getColumns()));
            $Column->appendAttribute('class', 'feature-tablehead');
            $ODiv = $Column->setOutputElement(new HtmlTag('div'));
            $ODiv->setAttribute('class', 'table-features');
            $H3 = $ODiv->add(new HtmlTag('h3'));
            $H3->setAttribute('class', 'admin-green-line');
            $H3->add(new HtmlStaticText('Features'));

            $LinkContainer = $ODiv->add(new HtmlTag('div'));
            $LinkContainer->setAttribute('class', 'links');

            foreach ($this->features as $ident => $state)
            {
                if (!$state || $ident == self::FEATURE_LIMITER)
                    continue;
                $this->appendFeatureLink($LinkContainer, $ident);
            }

            $hasAddIconClass = '';
            if (isset($this->features[self::FEATURE_ADDICON]) && $this->features[self::FEATURE_ADDICON])
            {
                $AddIconContainer = $ODiv->add(new HtmlTag('div'));
                $AddIconContainer->setAttribute('class', 'addIcon');
                $AddIconContainer->setAttribute('onclick' ,"window.location.href='" . FrontController::getInstance()->getLinkTo(null, null, 'new') . "';");

                $hasAddIconClass = ' withAddIcon';
            }

            if ((isset($this->features[self::FEATURE_LIMITER]) && $this->features[self::FEATURE_LIMITER]))
            {
                $LimiterContainer = $ODiv->add(new HtmlTag('div'));
                $LimiterContainer->setAttribute('class', 'limiter' . $hasAddIconClass);
                foreach ($this->limits as $limit)
                {
                    $Link = $LimiterContainer->add(new HtmlLink($limit, FrontController::getInstance()->getLinkTo(null,null,null, array('table' => $this->TableProperties->getIdent(),'limit' => $limit))));

                    if ($this->requestMethod == 'POST')
                    {
                        $Link->setAttribute('href', '#');
                        $Link->setAttribute('onclick', sprintf("blibsForm.setActionAndSubmitValue(event, '%s', %s);"
                                                        , FrontController::getInstance()->getLinkTo()
                                                        , StringFactory::buildJsLiteralNotation(array('refresh' => 0, 'table' => $this->TableProperties->getIdent(), 'limit' => $limit))
                                                      ));
                    }

                    if ($limit == $this->TableProperties->getLimit())
                        $Link->appendAttribute('class', 'active');
                }
            }


            return;
        }
    }
    // End setColumns

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Baut die Links auf die Zeile
     */
    protected function appendFeatureLink($Container, $ident)
    {
        switch ($ident)
        {
            case self::FEATURE_TIMEFRAME:
                $caption = $this->TableProperties->getTimeframeState() ? 'Timeframe off' : 'Timeframe on';
                $state = $this->TableProperties->getTimeframeState() ? '0' : '1';

                $Link = $Container->add(new HtmlLink($caption, FrontController::getInstance()->getLinkTo(null,null,null, array('table' => $this->TableProperties->getIdent(), 'timeframeState' => $state))));

                if ($this->requestMethod == 'POST')
                {
                    $Link->setAttribute('href', '#');
                    $Link->setAttribute('onclick', sprintf("blibsForm.setActionAndSubmitValue(event, '%s', %s);"
                                                    , FrontController::getInstance()->getLinkTo()
                                                    , StringFactory::buildJsLiteralNotation(array('refresh' => 0, 'table' => $this->TableProperties->getIdent(), 'timeframeState' => $state))
                                                  ));

                }
            break;
            case self::FEATURE_EXPORT:
                $Link = $Container->add(new HtmlLink('Export', FrontController::getInstance()->getLinkTo(null,null,'export')));
                if ($this->requestMethod == 'POST')
                {
                    $Link->setAttribute('href', '#');
                    $Link->setAttribute('onclick', sprintf("blibsForm.setActionAndSubmitValue(event, '%s', %s);"
                                                    , FrontController::getInstance()->getLinkTo(null, null, 'export')
                                                    , StringFactory::buildJsLiteralNotation(array('refresh' => 0, 'table' => $this->TableProperties->getIdent()))
                                                  ));
                }
            break;
            case self::FEATURE_FILTER:
                $caption = $this->TableProperties->getFilterState() ? 'Filter off' : 'Filter on';
                $state = $this->TableProperties->getFilterState() ? '0' : '1';
                $Link = $Container->add(new HtmlLink($caption, FrontController::getInstance()->getLinkTo(null,null,null, array('table' => $this->TableProperties->getIdent(), 'filterState' => $state))));

                if ($this->requestMethod == 'POST')
                {
                    $Link->setAttribute('href', '#');
                    $Link->setAttribute('onclick', sprintf("blibsForm.setActionAndSubmitValue(event, '%s', %s);"
                                                    , FrontController::getInstance()->getLinkTo()
                                                    , StringFactory::buildJsLiteralNotation(array('refresh' => 0, 'table' => $this->TableProperties->getIdent(), 'filterState' => $state))
                                                  ));
                }

                if ($this->TableProperties->getFilterState())
                {
                    $Link = $Container->add(new HtmlLink('Refresh filter', '#'));
                    $Link->setAttribute('onclick', "return blibsForm.setActionAndSubmit(event, '" . FrontController::getInstance()->getLinkTo() . "');");
                }
            break;
        }
    }
    // End appendFeatureLink

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableFeaturesRow
