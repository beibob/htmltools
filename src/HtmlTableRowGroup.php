<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * A html table row group
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 * @author     Thorsten M�rell <thorsten.muerell@beibob.net>
 * @abstract
 *
 */
abstract class HtmlTableRowGroup extends HtmlElement
{
    /**
     * Table
     */
    protected $Table;

    /**
     * Table rows
     */
    protected $tableRows = [];

    /**
     * Element name
     */
    protected $elementName;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new html table row group
     *
     * @param  HtmlTable $Table       the html table
     * @param  string $elementName    the name of the row group ('tfoot', 'thead', 'tbody')
     */
    public function __construct(HtmlTable $Table, $elementName = 'tbody')
    {
        $this->setTable($Table);
        $this->elementName = $elementName;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the table
     *
     * @param  HtmlTable $Table
     */
    public function setTable(HtmlTable $Table)
    {
        $this->Table = $Table;
    }
    // End setTable

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the table
     *
     * @param  HtmlTable $Table
     */
    public function getTable()
    {
        return $this->Table;
    }
    // End getTable

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a table row
     *
     * @param  HtmlTableRow $TableRow      null if you want to have one autocreated
     * @return HtmlTableRow                the created row
     */
    public function addTableRow(HtmlTableRow $TableRow = null)
    {
        if(is_null($TableRow))
            $TableRow = new HtmlTableRow();

        /**
         * Check if columns are already defined on the TableRow
         */
        if(!$TableRow->hasColumns())
            $TableRow->setColumns($this->Table->getColumns());

        return $this->tableRows[] = $TableRow;
    }
    // End addTableRow

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds the row group
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $DOMDocument)
    {
        $RowGroupElt = $this->getRowGroupElt($DOMDocument);
        $rowCount = count($this->tableRows);

        foreach($this->tableRows as $index => $TableRow)
        {
            $TableRow->setRowCount($rowCount);
            $TableRow->setRowIndex($index);
            $TableRow->appendTo($RowGroupElt);
        }

        if(is_null($RowGroupElt->childNodes) || !$RowGroupElt->childNodes->length)
            return null;

        return $RowGroupElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the section container element
     *
     * @param  DOMDocument $DOMDocument - to build the document with
     * @return DOMNode
     */
    protected function getRowGroupElt(DOMDocument $DOMDocument)
    {
        if(!count($this->tableRows))
            $this->addTableRow();

        $RowGroupElt = $DOMDocument->createElement($this->elementName);

        foreach($this->getAttributes() as $attribute => $value)
            if($value)
                $RowGroupElt->setAttribute($attribute, utf8_encode($value));

        return $RowGroupElt;
    }
    // End getRowGroupElt

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableRowGroup
