<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\HtmlTools\Interfaces\Converter;
use DOMDocument;
use DOMNode;

/**
 * The class implements a series of Radioboxes created from a DbObjectSet.
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlDataSetRadiobox extends HtmlRadiobox
{
    /**
     * This is the name of the property for the value
     */
    private $valueProperty;
    
    /**
     * The converter chain for the value
     */
    private $valueConverterChain = [];
    
    /**
     * The data set from which the tags are generated.
     */
    private $DataSet;
    
    //////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Creates a new instance of this class
     *
     * @param DbObjectSet $DataSet          the data set from which the radioboxes are built
     * @param string $captionProperty       the property that is used to built the caption of the radioboxes
     * @param string $valueProperty         the property that is used to built the value of the radioboxes
     * @param Converter $CaptionConverter   the Converter that is used to built the caption of the radioboxes
     * @param Converter $ValueConverter     the Converter that is used to built the value of the radioboxes
     */
    public function __construct(DbObjectSet $DataSet, $captionProperty, $valueProperty, Converter $CaptionConverter = null, Converter $ValueConverter = null) {
        $this->setName($captionProperty);
        $this->clearConverters();
        if (!is_null($CaptionConverter)) {
            $this->addConverter($CaptionConverter);
        } else {
            $this->addConverter(new ObjectPropertyConverter());
        }
        $this->valueProperty = $valueProperty;
        if (!is_null($ValueConverter)) {
            $this->valueConverterChain[] = $CaptionConverter;
        } else {
            $this->valueConverterChain[] = new ObjectPropertyConverter();
        }
        $this->DataSet = $DataSet;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Returns the converted value (i.e. the value of the value property)
     *
     * @return string   the value
     */
    public function getConvertedValue()
    {
        $value = null;
        foreach ($this->valueConverterChain as $Converter) {
            $value = $Converter->convertToText($value, $this->getDataObject(), $this->valueProperty);
        }
        return $value;
    }
    // End getConvertedValue

    //////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $DivElt = $Document->createElement('div');
        
        $value = $this->getConvertedValue();
        
        $InputElt = $Document->createElement('input');
        $InputElt->setAttribute('type', 'radio');
        $InputElt->setAttribute('name', $this->getRadioGroup()->getName());
        $InputElt->setAttribute('value', $value);
        
        $groupValue = $this->getRadioGroup()->getConvertedTextValue();
        
        if ($value == $groupValue)
            $InputElt->setAttribute('checked', 'checked');
        $DivElt->appendChild($InputElt);
        
        $SpanElt = $Document->createElement('span');
        $SpanElt->appendChild($Document->createTextnode($this->getConvertedTextValue()));
        
        $DivElt->appendChild($SpanElt);

        /**
         * Set remaining attributes
         */
        $this->buildAndSetAttributes($DivElt, $this->getDataObject(), $this->getName());

        return $DivElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Appends the element to the given node
     *
     * @see HtmlElement::appendTo()
     */
    public function appendTo(DOMNode $DOMNode)
    {
        $DOMDocument = $DOMNode instanceOf DOMDocument? $DOMNode : $DOMNode->ownerDocument;
        
        foreach ($this->DataSet as $DataObject) {
            $this->setDataObject($DataObject);
            $HtmlElt = $this->build($DOMDocument);
            $DOMNode->appendChild($HtmlElt);
        }

        return $DOMNode;
    }
    // End appendTo

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlDataSetRadiobox
