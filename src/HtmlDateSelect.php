<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\HtmlTools\Interfaces\Converter;
use DOMDocument;

/**
 * This is a date selection component for HtmlForms.
 *
 * The date is displayed as three selectboxes for the day, month and
 * year. Optionally the time is also shown.
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlDateSelect extends HtmlFormElement
{
    private $_converterPart;

    /**
     * Whether the time fields should be shown.
     */
    private $showTime = false;
    /**
     * Whether the month field should have localized string entries
     * or simply the month number.
     */
    private $localized = false;

    /**
     * Range of year-Select
     */
    private $yearStart;
    private $yearStop;

    private $nullOptions = false;
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new HtmlDateSelect box.
     *
     * @param string $name                   the name of this box (i.e. the property)
     * @param boolean $showTime              whether to show time fields.
     * @param Converter $DefaultConverter    the converter to use
     * @param boolean $localized             whether the month names should be shown
     **/
    public function __construct($name, $showTime = false, Converter $DefaultConverter = null, $localized = true) {
        parent::__construct($name, null, false, $DefaultConverter);
        $this->showTime = $showTime;
        $this->localized = $localized;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the showTime propertys
     *
     * @return boolean   whether the time fields are shown
     **/
    public function getShowTime() {
        return $this->showTime;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the showTime property
     *
     * @param boolean $showTime   whether to show the time fields or not
     **/
    public function setShowTime($showTime) {
        $this->showTime = $showTime;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt Start und Stop-Datum der Jahresbox
     */
    public function setYearRange($start, $stop)
    {
        $this->setYearStart($start);
        $this->setYearStop($stop);
    }
    // End setYearRange

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt das Start-Datum der Jahresbox
     */
    public function setYearStart($year)
    {
        $this->yearStart = $year;
    }
    // End setYearStart

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt das Stop-Datum der Jahresbox
     */
    public function setYearStop($year)
    {
        $this->yearStop = $year;
    }
    // End setYearStop

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the showTime nullOptions
     *
     * @return boolean
     **/
    public function getNullOptions() {
        return $this->nullOptions;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the showTime nullOptions
     *
     * @return boolean
     **/
    public function setNullOptions($state = true) {
        $this->nullOptions = $state;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $Container = $Document->createElement('div');

        if ($this->localized) {
            $Container->appendChild($this->createDaySelect($Document));
            $Container->appendChild($this->createMonthSelect($Document));
            $Container->appendChild($this->createYearSelect($Document));
        } else {
            $Container->appendChild($this->createYearSelect($Document));
            $Container->appendChild($Document->createTextnode(' - '));
            $Container->appendChild($this->createMonthSelect($Document));
            $Container->appendChild($Document->createTextnode(' - '));
            $Container->appendChild($this->createDaySelect($Document));
        }

        if ($this->showTime) {
            $Container->appendChild($this->createHourSelect($Document));
            $Container->appendChild($Document->createElement('div', ':', ['class' => 'colon']));
            $Container->appendChild($this->createMinuteSelect($Document));
        }

        /**
         * Set remaining attributes
         */
        $this->buildAndSetAttributes($Container, $this->getDataObject(), $this->getName());

        return $Container;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks error
     */
    public function hasError()
    {
        if (!$this->hasForm() || !$this->getForm()->hasValidator() || !$this->getForm()->getValidator()->hasErrors())
            return false;

        if ($this->hasForm() && $this->getForm()->hasValidator())
        {
            $Validator = $this->getForm()->getValidator();
            if (   $Validator->hasError($this->getName() . '_year')
                || $Validator->hasError($this->getName() . '_month')
                || $Validator->hasError($this->getName() . '_day')
               )
                return true;
        }

        return false;
    }
    // End hasError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Disable adding of children here.
     */
    public function addChild(HtmlElement $Element)
    {
        throw new Exception("Cannot add children to a HtmlDateSelect.");
    }
    // End addChild

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value of the element
     *
     * @return mixed
     */
    public function getValidatedFormDataObject()
    {
        if ($this->hasForm() && $this->getForm()->hasRequest()) {
            $name = $this->getName();
            $Request = $this->getForm()->getRequest();
            $value = "";
            $value .= sprintf("%04d", $Request->get($name . "_year")) . "-";
            $value .= sprintf("%02d", $Request->get($name . "_month")) . "-";
            $value .= sprintf("%02d", $Request->get($name . "_day")) . " ";

            if (!$this->showTime) {
                $value .= "00:00:00";
            } else {
                $value .= sprintf("%02d", $Request->get($name . "_hour")) . ":";
                $value .= sprintf("%02d", $Request->get($name . "_minute")) . ":00";
            }
        }
        else
        {
            $value = $this->getValue();
        }

        $timestamp = ($value == '' & $this->getNullOptions()) ? null : strtotime($value);
        $a =  $this->getValidatedFormDataObjectForValue($timestamp);

        return $a;
    }
    // End getValidatedFormDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the raw text value after calling the converter chain
     *
     * @return string    the text value
     */
    public function getConvertedTextValue($value = null)
    {
        $name = $this->getName() . '_' . $this->_converterPart;

        /**
         * Prefer request value
         */
        if ($this->hasForm() &&
            $this->getForm()->hasRequest() &&
            $this->getForm()->getRequest()->has($name))
        {
            return $this->getForm()->getRequest()->$name;
        }

        return parent::getConvertedTextValue(!is_null($value)? $value : $this->getValue());
    }
    // End getConvertedTextValue

    //////////////////////////////////////////////////////////////////////////////////////
    // private
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the year select box.
     *
     * @param \DOMDocument $DomDocument   the document to build the box with
     * @return \DOMNode                    the created DOM node
     */
    private function createYearSelect(DOMDocument $Document)
    {
        $this->_converterPart = 'year';
        if ($this->getNullOptions() && $this->getConvertedTextValue() == '')
            $value = '';
        else
            $value = $this->getConvertedTextValue();

        if (strpos($value, '-'))
        {
            $parts = explode('-', $value);
            $value = (int) $parts[0];
        }

        $startYear = !is_null($this->yearStart) ? $this->yearStart : date('Y') - 10;
        $stopYear = !is_null($this->yearStop) ? $this->yearStop : date('Y') + 10;

        return $this->createSelect($Document, $this->getName(), "year", $startYear, $stopYear, $value);
    }
    // End createYearSelect

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the month select box.
     *
     * @param  \DOMDocument $DomDocument   the document to build the box with
     * @return \DOMNode                    the created DOM node
     */
    private function createMonthSelect(DOMDocument $Document)
    {
        $this->_converterPart = 'month';
        if ($this->getNullOptions() && $this->getConvertedTextValue() == '')
            $value = '';
        else
            $value = $this->getConvertedTextValue();

        if (strpos($value, '-'))
        {
            $parts = explode('-', $value);
            $value = (int) $parts[1];
        }

        $month_names = [];

        return $this->createSelect($Document, $this->getName(), "month", 1, 12, $value, $month_names);
    }
    // End createMonthSelect

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the day select box.
     *
     * @param  DOMDocument $DomDocument   the document to build the box with
     * @return DOMNode                    the created DOM node
     */
    private function createDaySelect(DOMDocument $Document)
    {
        $this->_converterPart = 'day';
        if ($this->getNullOptions() && $this->getConvertedTextValue() == '')
            $value = '';
        else
            $value = $this->getConvertedTextValue();

        if (strpos($value, '-'))
        {
            $parts = explode(' ', $value);
            $parts = explode('-', array_shift($parts));
            $value = (int) $parts[2];
        }

        return $this->createSelect($Document, $this->getName(), "day", 1, 31, $value);
    }
    // End createDaySelect

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the hour select box.
     *
     * @param  DOMDocument $DomDocument   the document to build the box with
     * @return DOMNode                    the created DOM node
     */
    private function createHourSelect(DOMDocument $Document)
    {
        $this->_converterPart = 'hour';
        if ($this->getNullOptions() && $this->getConvertedTextValue() == '')
            $value = '';
        else
            $value = $this->getConvertedTextValue();

        if (strpos($value, '-'))
        {
            $parts = explode(' ', $value);
            $parts = explode(':', $parts[1]);
            $value = (int) $parts[0];
        }

        $better = [];
        for ($i = 0; $i <= 23; $i++)
            $better[$i] = str_pad($i, 2, '0', STR_PAD_LEFT);

        return $this->createSelect($Document, $this->getName(), "hour", 1, 23, $value, $better);
    }
    // End createHourSelect

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the minute select box.
     *
     * @param  DOMDocument $DomDocument   the document to build the box with
     * @return DOMNode                    the created DOM node
     */
    private function createMinuteSelect(DOMDocument $Document)
    {
        $this->_converterPart = 'minute';
        if ($this->getNullOptions() && $this->getConvertedTextValue() == '')
            $value = '';
        else
            $value = $this->getConvertedTextValue();

        if (strpos($value, '-'))
        {
            $parts = explode(' ', $value);
            $parts = explode(':', $parts[1]);
            $value = $parts[1];
        }

        $better = [];
        for ($i = 0; $i <= 59; $i++)
            $better[$i] = str_pad($i, 2, '0', STR_PAD_LEFT);

        return $this->createSelect($Document, $this->getName(), 'minute', 0, 59, $value, $better);
    }
    // End createMinuteSelect

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a select box.
     *
     * @param  DOMDocument $Document    the document to build the box with
     * @param  string $name             the name of the HtmlDateSelect
     * @param  string $type             the type of the box
     * @param  int $begin               the start count for the box entries
     * @param  int $end                 the end count for the box entries
     * @param  mixed $currentValue      the current value of the box
     * @param  array $values            an array with the textual values for the entries
     * @return DOMNode                  the created DOM node
     */
    private function createSelect(DOMDocument $Document, $name, $type, $begin, $end, $currentValue, $values = null)
    {
        $SelectElt = $Document->createElement('select');

        $SelectElt->setAttribute('name', $name . "_" . $type);

        if ($this->getNullOptions())
        {
            $OptionElt = $SelectElt->appendChild($Document->createElement('option'));
            $OptionElt->setAttribute('value', '');
            $OptionElt->appendChild($Document->createTextnode('--'));
        }

        for ($i = $begin; $i <= $end; $i++) {
            $Option = $Document->createElement('option');
            $Option->setAttribute('value', $i);
            if ((int)$currentValue === $i && $currentValue != '') {
                $Option->setAttribute('selected', 'selected');
            }
            if (is_null($values) || empty($values))
                $Option->appendChild($Document->createTextnode($i));
            else
                $Option->appendChild($Document->createTextnode($values[$i % count($values)]));
            $SelectElt->appendChild($Option);
        }

        if ($this->isReadonly())
            $SelectElt->setAttribute('readonly', 'readonly');

        $SelectElt->setAttribute('class', 'dateSelect'. ucfirst($type));

        return $SelectElt;
    }
    // End createSelect

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlDateSelect
