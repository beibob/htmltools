<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Simple Html element containing just one entity reference
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 */
class HtmlEntityReference extends HtmlDataElement
{
    /**
     * The entity
     */
    private $entity;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new static entity
     *
     * @param string $entity
     */
    public function __construct($entity)
    {
        $this->entity = $entity;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        return $Document->createEntityReference($this->entity);
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlStaticText
