<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 *
 */
class HtmlTableProperties
{
    const DEFAUL_LIMIT = 10;
    private static $Instances = array();

    const SET_FILTERVALUE_CONTEXT_UNKNOWN = 0;
    const SET_FILTERVALUE_CONTEXT_CONSTRUCT = 1;
    const SET_FILTERVALUE_CONTEXT_ADDING = 2;
    const SET_FILTERVALUE_CONTEXT_REQUEST = 3;
    const SET_FILTERVALUE_CONTEXT_DISABLED = 4;

    private $Session;
    private $ident;
    private $Namespace;
    private $DataSet;
    private $filters = array();

    /**
     *
     */
    public static function getInstance($ident, array $defaultOrderBy = null)
    {
        if (!isset(self::$Instances[$ident]))
            self::$Instances[$ident] = new HtmlTableProperties($ident, $defaultOrderBy);

        return self::$Instances[$ident];
    }
    // End getInstance

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the ident
     */
    public function getIdent()
    {
        return $this->ident;
    }
    // End getIdent

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function handleRequest(HttpRequest $Request)
    {
        // Ja bin i denn zuständig?
        if ($Request->table != $this->getIdent())
            return;

        // Anzahl Zeilen pro Seite setzen
        if ($Request->limit)
        {
            $this->setLimit($Request->limit);
            $this->setOffset(0);
        }

        // Sortierrichtung
        if ($Request->sort && $Request->dir)
            $this->setOrderBy(array($Request->sort => $Request->dir));

        // Blättern
        if (isset($Request->page) && $Request->page <= $this->getPageCount())
            $this->setOffset($Request->page * $this->getLimit());

        // Filter an/aus
        if (isset($Request->filterState))
        {
            $this->setFilterState($Request->filterState);
            if (!$this->getFilterState())
                $this->filters = $this->Namespace->filters = array();
        }

        // Filter aktualisieren
        if ($Request->refresh)
        {
            $filterValues = $Request->filter;
            foreach($this->filters as $property => $Filter)
            {
                if (isset($Request->filter[$property]))
                {
                    $Filter->setFilterValue($Request->filter[$property], self::SET_FILTERVALUE_CONTEXT_REQUEST);
                    $this->addFilter($Filter);
                }
            }
            $this->setOffset(0);
        }

        // Timeframe an/aus
        if (isset($Request->timeframeState))
        {
            $this->setTimeframeState($Request->timeframeState);
            if (!$this->getTimeframeState())
                $this->Namespace->tsStart = $this->Namespace->tsStop = false;
        }

        // Zeitliche Eingrenzung
        if (isset($Request->timeframeMode))
        {
            $tsObjects = HtmlTimeframeInputElements::getDateTimeObjectsByRequest($Request);
            if (!count($tsObjects['errors']))
            {
                $this->Namespace->tsStart = is_object($tsObjects['start']) ? $tsObjects['start']->getISODateTimeString() : false;
                $this->Namespace->tsStop = is_object($tsObjects['stop']) ? $tsObjects['stop']->getISODateTimeString() : false;
                $this->setOffset(0);
            }
        }
    }
    // End handleRequest

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function addFilter(HtmlTableFilter $Filter, $force = false)
    {
        $filters = $this->Namespace->filters;
        if (!is_array($filters))
            $filters = array();

        if (!$force && is_null($Filter->getFilterValue()) && isset($filters[$Filter->getFilterProperty()]) && !is_null($filters[$Filter->getFilterProperty()]['value']))
            $Filter->setFilterValue($filters[$Filter->getFilterProperty()]['value'], self::SET_FILTERVALUE_CONTEXT_ADDING);

        $filters[$Filter->getFilterProperty()] = array(  'property' => $Filter->getFilterProperty()
                                                       , 'value' => $Filter->getFilterValue()
                                                       , 'type' => get_class($Filter));

        $this->Namespace->filters = $filters;

        $this->filters[$Filter->getFilterProperty()] = $Filter;
        $Filter->setTableProperties($this);
        return $Filter;
    }
    // End addFilter

    ///////////////////////////////////////////////////////////////////////////////////////////
    public function refreshFiltersInSession()
    {
        $filters = array();
        foreach ($this->getFilters() as $property => $Filter)
        {
            $filters[$Filter->getFilterProperty()] = array(  'property' => $Filter->getFilterProperty()
                                                           , 'value' => $Filter->getFilterValue()
                                                           , 'type' => get_class($Filter));
        }

        $this->Namespace->filters = $filters;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt einen Filter auf NULL
     */
    public function disableFilter(HtmlTableFilter $Filter)
    {
        $Filter->setFilterValue(null, self::SET_FILTERVALUE_CONTEXT_DISABLED);

        $filters = $this->Namespace->filters;
        if (!is_array($filters))
            $filters = array();

        $filters[$Filter->getFilterProperty()] = array(  'property' => $Filter->getFilterProperty()
                                                       , 'value' => $Filter->getFilterValue()
                                                       , 'type' => get_class($Filter));

        $this->Namespace->filters = $filters;

        return $this->addFilter($Filter);
    }
    // End disableFilter

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Entfernt den übergebenen Filter
     */
    public function removeFilter(HtmlTableFilter $Filter)
    {
        $filters = $this->Namespace->filters;
        if (!is_array($filters))
            return;

        if (!isset($filters[$Filter->getFilterProperty()]))
            return;

           unset($filters[$Filter->getFilterProperty()]);
        unset($this->filters[$Filter->getFilterProperty()]);

        $this->Namespace->filters = $filters;
    }
    // End removeFilter

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  Entfernt einen Filter an Hand seiner property
     */
    public function removeFilterByProperty($property)
    {
        if (!$property)
            return;

        foreach ($this->getFilters() as $Filter)
        {
            if ($Filter->getFilterProperty() == $property)
                return $this->removeFilter($Filter);
        }
    }
    // End removeFilterByProperty

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getFilters()
    {
        return $this->filters;
    }
    // End getFilters

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }
    // End setFilters

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  Liefert ein Filter-Objekt an Hand seiner property
     */
    public function getFilterByProperty($property)
    {
        if (!isset($this->filters[$property]))
            return false;

        return $this->filters[$property];
    }
    // End getFilterByProperty

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getFilterState()
    {
        return $this->Namespace->filterState;
    }
    // End getFilterState

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setFilterState($state = true)
    {
        return $this->Namespace->filterState = $state;
    }
    // End setFilterState

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the current filters as sql fragment
     */
    public function getFilterSql()
    {
        if (!count($this->getFilters()))
            return false;

        $parts = $this->getFilterSqlParts();

        if (!count($parts))
            return false;

        $sql = join(' AND ', $parts);
        return '(' . $sql .')';
    }
    // End getFilterSql

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the current filters as sql fragment
     */
    public function getFilterSqlParts()
    {
        $parts = array();
        if (!count($this->getFilters()))
            return $parts;

        foreach ($this->getFilters() as $Filter)
        {
            if ($filterSql = $Filter->getFilterSql())
                $parts[$Filter->getFilterProperty()] = $filterSql;
        }

        return $parts;
    }
    // End getFilterSql

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getTimeframeState()
    {
        return $this->Namespace->timeframeState;
    }
    // End getTimeframeState

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setTimeframeState($state = true)
    {
        return $this->Namespace->timeframeState = $state;
    }
    // End setTimeframeState

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setTimeframe($start, $stop)
    {
        $this->Namespace->tsStart = $start;
        $this->Namespace->tsStop = $stop;

        return $this->getTimeframeArray();
    }
    // End setTimeframe

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getTimeframeArray()
    {
        return array('start' => $this->Namespace->tsStart, 'stop' => $this->Namespace->tsStop);
    }
    // End getTimeframeArray

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the begin of timeframe
     */
    public function getTimeframeStart()
    {
        $tsArray = $this->getTimeframeArray();
        return $tsArray['start'];
    }
    // End getTimeframeStart

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the end of timeframe
     */
    public function getTimeframeStop()
    {
        $tsArray = $this->getTimeframeArray();
        return $tsArray['stop'];
    }
    // End getTimeframeStop

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getTimeframeSql($timeframeProperty = 'created')
    {
        $timeframeArray = $this->getTimeframeArray();
        if (!$timeframeArray['start'] || !$timeframeArray['stop'])
            return false;

        return sprintf("(%s BETWEEN '%s' AND '%s')", $timeframeProperty, $timeframeArray['start'], $timeframeArray['stop']);
    }
    // End getTimeframeSql

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the current order by as an array
     */
    public function getOrderBy()
    {
        if (!is_array($this->Namespace->orderBy))
            $this->Namespace->orderBy = array();

        return count($this->Namespace->orderBy) ? $this->Namespace->orderBy : array();
    }
    // End getOrderBy

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the current order by as an array
     */
    public function setOrderBy(array $orderBy = array())
    {
        return $this->Namespace->orderBy = $orderBy;
    }
    // End setOrderBy

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current (first) column to order by
     */
    public function getOrderColumn()
    {
        if (!is_array($this->Namespace->orderBy))
            $this->Namespace->orderBy = array();

        if (!count($this->Namespace->orderBy))
            return false;

        $orderBy = array_keys($this->Namespace->orderBy);
        return array_shift($orderBy);
    }
    // End getOrderColumn

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current (first) column to order by
     */
    public function getOrderDir()
    {
        if (!is_array($this->Namespace->orderBy))
            $this->Namespace->orderBy = array();

        $orderBy = $this->Namespace->orderBy;
        return count($orderBy) ? array_shift($orderBy) : false;
    }
    // End getOrderBy

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the current limit
     */
    public function getLimit()
    {
        if (!isset($this->Namespace->limit))
            $this->Namespace->limit = false;

        return $this->Namespace->limit;
    }
    // End getLimit

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * returns the current limit
     */
    public function setLimit($limit)
    {
        return $this->Namespace->limit = $limit;
    }
    // End setLimit

    ///////////////////////////////////////////////////////////////////////////////////////////
    public function setOffset($offset)
    {
        return $this->Namespace->offset = $offset;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current Offset
     */
    public function getOffset()
    {
        return $this->Namespace->offset;
    }
    // End getOffset

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the current data set
     */
    public function getDataSet()
    {
        return $this->DataSet;
    }
    // End getDataSet

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * sets setDataSet
     */
    public function setDataSet(DbObjectSet $Set)
    {
        return $this->DataSet = $Set;
    }
    // End setDataSet

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the total amount of records
     */
    public function getRecordCount()
    {
        return $this->Namespace->recordCount;
    }
    // End getRecordCount

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setRecordCount($recordCount)
    {
        return $this->Namespace->recordCount = $recordCount;
    }
    // End setRecordCount

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getPageCount()
    {
        if (!$this->getLimit())
            return $this->getRecordCount() ? 1 : 0;

        return ceil($this->getRecordCount() / $this->getLimit());
    }
    // End getPageCount

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getPageIndex()
    {
        if (!$this->getLimit())
            return 0;

        return floor($this->getOffset() / $this->getLimit());
    }
    // End getPageIndex

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setPageIndex($index)
    {
        $index = (int) $index;
        if ($index >= 0 && $index <= $this->getPageCount())
            $this->setOffset($index * $this->getLimit());
    }
    // End setPageIndex

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    private function __construct($ident, array $defaultOrderBy = null)
    {
        $this->ident = $ident;
        $this->Session = FrontController::getInstance()->getSession();
        $this->Namespace = $this->Session->getNamespace('HtmlTableProperties-' . $this->ident, true);
        $this->initFilters();

        if (!is_null($defaultOrderBy) && (!is_array($this->Namespace->orderBy) || !count($this->Namespace->orderBy)))
            $this->setOrderBy($defaultOrderBy);
    }
    // __construct

    ///////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    private function initFilters()
    {
        if (!is_array($this->Namespace->filters))
            $this->Namespace->filters = array();

        foreach ($this->Namespace->filters as $property => $filterConf)
            $this->filters[$property] = new $filterConf['type']($filterConf['property'], $filterConf['value']);
    }
    // End initFilters

    ///////////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableProperties
