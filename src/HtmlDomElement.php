<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Simple Html element containing some xml snippet
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlDomElement extends HtmlElement
{
    private $domText;
    
    /**
     * Creates a new static text element
     *
     * @param string $value the text to display
     */
    public function __construct($domText) {
        $this->domText = $domText;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $f = $Document->createDocumentFragment();
        $f->appendXML($this->domText);

        return $f;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlDomElement
