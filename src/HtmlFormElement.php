<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\DbObject;
use Beibob\HtmlTools\Interfaces\Converter;

/**
 * This is the base class for all HTML form elements
 *
 * Form elements have a property for the form they are attached to
 * and an id.
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 * @author Tobias Lode <tobias@beibob.de>
 * @abstract
 *
 */
abstract class HtmlFormElement extends HtmlEditableElement
{
    /**
     * The form this element belongs to
     */
    private $Form = null;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new form element
     *
     * @param  string $name                 the name of the form element (i.e. the property)
     * @param  mixed $value                 value override for the form element
     * @param  boolean $readonly            whether this element is readonly
     * @param  Converter $DefaultConverter  the default converter to use
     * @param  DbObject $DataObject         the data object to use
     */
    public function __construct($name, $value = null, $readonly = false, Converter $DefaultConverter = null, DbObject $DataObject = null)
    {
        parent::__construct($name, $value, $readonly, $DefaultConverter, $DataObject);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the associated form
     *
     * @param  HtmlForm $Form
     */
    public function setForm(HtmlForm $Form)
    {
        $this->Form = $Form;
    }
    // End setForm

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the associated form
     *
     * @return HtmlForm the form
     */
    public function getForm()
    {
        return $this->Form;
    }
    // End getForm

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks if a form parent is set
     *
     * @return boolean if the form ist set
     */
    public function hasForm()
    {
        return !is_null($this->Form);
    }
    // End hasForm

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks error
     */
    public function hasError()
    {
        if (!$this->hasForm() || !$this->getForm()->hasValidator() || !$this->getForm()->getValidator()->hasErrors())
            return false;

        if ($this->hasForm() && $this->getForm()->hasValidator() && $this->getForm()->getValidator()->hasError($this->getName()))
            return true;

        return false;
    }
    // End hasError

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the form object recursive on all children that are HtmlFormElements
     *
     * @param  HtmlElement $Element      the html element to start with
     * @param  HtmlForm $Form            the form
     */
    public static function setFormRecursive(HtmlElement $Element, HtmlForm $Form = null)
    {
        if($Element instanceof HtmlFormElement && !is_null($Form))
            $Element->setForm($Form);

        foreach($Element->getChildren() as $Child)
        {
            if($Child instanceof HtmlFormElement)
                $Child->setForm($Form);

            self::setFormRecursive($Child, $Form);
        }
    }
    // End setFormRecursive

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a child to the list of children.
     *
     * This override changes a little bit of the semantic of addChild
     * to return the first HtmlFormElement of the $Element.
     *
     * If none is found, the $Element is returned.
     *
     * @param  HtmlElement $Element   The child to add
     * @return HtmlElement the first children with class HtmlFormElement or $Element
     */
    public function addChild(HtmlElement $Element)
    {
        parent::addChild($Element);
        HtmlFormElement::setFormRecursive($Element, $this->getForm());

        if($Element instanceof HtmlFormElement)
            return $Element;

        $SearchResult = self::searchFirstFormElementChild($Element);

        if (!is_null($SearchResult))
            return $SearchResult;

        return $Element;
    }
    // End addChild

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * This function returns the first child in the children tree of $Element which
     * is of the type HtmlFormElement.
     *
     * @param HtmlElement $Element   the element to search
     * @return HtmlFormElement       or null
     **/
    public static function searchFirstFormElementChild(HtmlElement $Element) {
        foreach ($Element->getChildren() as $Child) {
            if ($Child instanceof HtmlFormElement)
                return $Child;
            $Ret = self::searchFirstFormElementChild($Child);
            if (!is_null($Ret))
                return $Ret;
        }
        return null;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the raw text value after calling the converter chain
     *
     * @return string    the text value
     */
    public function getConvertedTextValue($value = null)
    {
        $name = $this->getName();
        $isArray = false;

        // check for arrays
        if(preg_match('/^(.+?)\[(.+?)\]$/', $name, $matches))
        {
            $name = $matches[1];
            $key  = $matches[2];
            $isArray = true;
        }

        /**
         * Prefer request value
         */
        if ($this->hasForm() &&
            $this->getForm()->hasRequest() &&
            $this->getForm()->getRequest()->has($name))
        {
            $mixed = $this->getForm()->getRequest()->$name;

            if($isArray)
            {
                if(is_array($mixed) && isset($mixed[$key]))
                    return $mixed[$key];

                // fall back to value
                return parent::getConvertedTextValue(!is_null($value)? $value : $this->getValue());
            }

            return $mixed;
        }

        return parent::getConvertedTextValue(!is_null($value)? $value : $this->getValue());
    }
    // End getConvertedTextValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value of the element
     *
     * @return mixed
     */
    public function getValidatedFormDataObject()
    {
        if ($this->hasForm() && $this->getForm()->hasRequest())
        {
            $name = $this->getName();
            $Request = $this->getForm()->getRequest();
            $value = $Request->has($name)? $Request->$name : $this->getValue();
        }
        else
        {
            $value = $this->getValue();
        }
        return $this->getValidatedFormDataObjectForValue($value);
    }
    // End getValidatedFormDataObject

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlFormElement
