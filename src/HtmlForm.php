<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\Interfaces\Request;
use Beibob\Blibs\Validator;
use DOMDocument;

/**
 * This class represents a HTML form
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlForm extends HtmlElement
{
    /**
     * Validator
     */
    private $Validator;

    /**
     * Datenobjekt
     */
    private $DataObject;

    /**
     * das HTTP request object
     */
    private $Request = null;

    /**
     * ReadOnly-Flag for all contained form elements
     */
    private $readonly;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new HTML form
     *
     * @param  string $formName    the name of this form
     * @param  string $action      the action of this form
     * @param  string $method      the method (either POST or GET)
     */
    public function __construct($name, $action, $method = 'POST')
    {
        $this->setName($name);
        $this->setAction($action);
        $this->setMethod($method);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the form name
     *
     * @param  string $name
     */
    public function setName($name)
    {
        $this->setAttribute('name', $name);
    }
    // End setName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the form action
     *
     * @param  string $action
     */
    public function setAction($action)
    {
        $this->setAttribute('action', $action);
    }
    // End setAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the form method
     *
     * @param  string $method
     */
    public function setMethod($method)
    {
        $this->setAttribute('method', $method);
    }
    // End setMethod

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the form validator
     *
     * @param  Validator $Validator
     */
    public function setValidator(Validator $Validator)
    {
        return $this->Validator = $Validator;
    }
    // End setValidator

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the DataObject
     *
     * @param  \stdClass $DataObject
     */
    public function setDataObject(\stdClass $DataObject)
    {
        return $this->DataObject = $DataObject;
    }
    // End setDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the HTTP Request
     *
     * @param  Request $Request the request object
     * @param  Request the just set request
     */
    public function setRequest(Request $Request)
    {
        return $this->Request = $Request;
    }
    // End setRequest

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the request object
     *
     * @return Request the HTTP request
     */
    public function getRequest()
    {
        return $this->Request;
    }
    // End getRequest

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Tests whether a request object is set
     *
     * @return boolean true if the request object is set
     */
    public function hasRequest()
    {
        return !is_null($this->Request);
    }
    // End hasRequest

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the form name
     */
    public function getName()
    {
        return $this->getAttribute('name');
    }
    // End getName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the action
     */
    public function getAction()
    {
        return $this->getAttribute('action');
    }
    // End getAction

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the form method
     */
    public function getMethod()
    {
        return $this->getAttribute('method');
    }
    // End getMethod

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if a validator was set
     *
     * @return boolean
     */
    public function hasValidator()
    {
        return $this->Validator instanceOf Validator;
    }
    // End hasValidator

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns a Validator
     *
     * @return Validator
     */
    public function getValidator()
    {
        if($this->hasValidator())
            return $this->Validator;

        return new Validator();
    }
    // End getValidator

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the readonly-flag
     *
     * @param  bool $readonly
     */
    public function setReadonly($readonly = true)
    {
        $this->readonly = $readonly;
    }
    // End setReadonly

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the state of readonly-Flag
     *
     * @return bool
     */
    public function isReadonly()
    {
        return $this->readonly;
    }
    // End isReadonly

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert das Datenobjekt
     *
     * @return \stdClass
     */
    public function getDataObject()
    {
        return $this->DataObject;
    }
    // End getDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert ein neues Datenobjekt, in dem alle Felder je nach den Convertern der
     * der einzelnen HtmlEditableElements gef�llt sind.
     *
     * Die Funktion ruft convertFromText auf allen HtmlEditableElements auf und gibt das
     * Objekt zur�ck.
     *
     * @return \stdClass
     */
    public function getValidatedFormDataObject()
    {
        HtmlDataElement::setDataObjectRecursive($this, $this->getDataObject());
        HtmlDataElement::recursiveGetValidatedFormDataObject($this);
        return $this->getDataObject();
    }
    // End getValidatedFormDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value of a html data element within the current form
     *
     * @param  string $name
     * @return mixed
     */
    public function getElementValue($name)
    {
        if($this->hasValidator() && $this->Validator->getValue($name))
            return $this->Validator->getValue($name);

        if($this->hasRequest() && $this->Request->has($name))
            return $this->Request->$name;

        $DataObject = $this->getDataObject();
        return isset($DataObject->$name)? $DataObject->$name : null;

        /* if(!$DataObject = $this->getValidatedFormDataObject()->getDataObject()) */
        /*     return null; */

        /* return isset($DataObject->$name)? $DataObject->$name : null; */
    }
    // End getElementValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds an HtmlElement as a child to this form.
     *
     * This method has a slightly changed semantic, it returns the first
     * HtmlFormElement in the tree of $Element.
     *
     * @param  HtmlElement $Element    the element or tree to add
     * @return HtmlElement             the first HtmlFormElement or $Element itself
     */
    public function addChild(HtmlElement $Element)
    {
        parent::addChild($Element);

        HtmlFormElement::setFormRecursive($Element, $this);

        if ($Element instanceof HtmlFormElement)
            return $Element;

        $SearchResult = HtmlFormElement::searchFirstFormElementChild($Element);

        if (!is_null($SearchResult))
            return $SearchResult;

        return $Element;
    }
    // End addChild

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $FormElt = $Document->createElement('form');

        $this->setAttribute('accept-charset', 'utf-8');
        $this->appendAttribute('class', $this->getName());
        $this->buildAndSetAttributes($FormElt);

        $DataObject = $this->getDataObject();
        $Validator  = $this->getValidator();

        HtmlDataElement::setDataObjectRecursive($this, $DataObject);
        HtmlFormElement::setFormRecursive($this, $this);

        if($this->readonly)
            HtmlEditableElement::setReadonlyRecursive($this, true);

        foreach($this->getChildren() as $Element)
        {
            //$Element->setValidator($Validator);
            $Element->appendTo($FormElt);
        }

        return $FormElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlForm
