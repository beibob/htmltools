<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * An element which creates a HTML image
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class HtmlImage extends HtmlElement
{
    /**
     * Creates a new static image element
     *
     * @param string $src
     * @param int    $width
     * @param int    $height
     * @param string $alt
     */
    public function __construct($src, $width = null, $height = null, $alt = '')
    {
        $this->setAttribute('src', $src);
        $this->setAttribute('width', $width);
        $this->setAttribute('height', $height);
        $this->setAttribute('alt', $alt);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Overwrite to prevent adding children to the img element
     *
     * @see HtmlElement::addChild
     *
     * @param  HtmlElement $Element   The child to add
     * @return HtmlElement   The element just added
     */
    public function addChild(HtmlElement $Element) {
        return $Element;
    }
    // End addChild

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element.
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $Img = $Document->createElement('img');
        $this->buildAndSetAttributes($Img);

        return $Img;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlImage
