<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * Baut ein Fieldset mit einem <div class="legend"/>-tag als legend
 * um den CSS Problemen mit dem legend-Tag zu entkommen.
 *
 * Der besseren Benamung wegen sollte dieses Element nicht direkt benutzt
 * werden, sondern eine der beiden Ableitungen
 *
 * HtmlFormFieldsetLegend
 * oder
 * HtmlFormFieldsetDivLegend
 *
 * <code>
 *   $FormGroup = $Form->add(new HtmlFormGroup('Name'));
 *   $FormGroup->add(...)
 * </code>
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlFormGroup extends HtmlFormElement
{
    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $this->appendAttribute('class', 'fieldset');

        $GroupDiv = $Document->createElement('div');
        $this->buildAndSetAttributes($GroupDiv);

        $Fieldset = $GroupDiv->appendChild($Document->createElement('fieldset'));

        $this->buildNameElement($Fieldset);

        foreach ($this->getChildren() as $Child) {
            $Child->appendTo($Fieldset);
        }

        return $GroupDiv;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Haengt den Namen der Gruppe als legend-Tag in den Container
     */
    protected function buildNameElement($Container)
    {
        if($this->getName())
        {
            $Legend = $Container->ownerDocument->createElement('div');
            $Legend->setAttribute('class', 'legend');
            $Legend->appendChild($Container->ownerDocument->createTextnode($this->getName()));
            $Container->appendChild($Legend);
        }
    }
    // End buildNameElement

    //////////////////////////////////////////////////////////////////////////////////////

    // Disable this for a form group.
    public function getValidatedFormDataObject() { return null; }

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlFormGroup
