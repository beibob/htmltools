<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 * A html table head row
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 * @author     Thorsten M�rell <thorsten.muerell@beibob.net>
 *
 */
class HtmlTableHeadRow extends HtmlTableRow
{
    /**
     * Sets the table columns
     *
     * @param  array $columns
     */
    public function setColumns(array $columns)
    {
        parent::setColumns($columns);

        $DataObject = (object)null;

        foreach($columns as $name => $column)
        {
            $DataObject->$name = $column->getCaption();

            /**
             * Set name also as class name
             */
            $this->getColumn($name)->appendAttribute('class', $name);
        }

        $this->setTableCellType('th');
        $this->setDataObject($DataObject);
    }
    // End setColumns

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTableRow
