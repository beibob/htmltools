<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * NOT YET FUNCTIONAL!!!
 * @ignore
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 *
 */
class HtmlLimiter extends HtmlElement
{
    /**
     * ActionLink
     */
    private $actionLink;

    /**
     * Limits
     */
    private $limits = [10, 20, 50];

    /**
     * Current limit
     */
    private $limit;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the action links
     *
     * @param  string $actionLink
     * @return -
     */
    public function setActionLink($link)
    {
        return $this->actionLink = $link;
    }
    // End setActionLink

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets offered limits
     *
     * @param  array $limits
     * @return array
     */
    public function setLimits(array $limits)
    {
        return $this->limits = $limits;
    }
    // End setLimits

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets active limit
     *
     * @param  int $limit
     * @return int
     */
    public function setLimit($limit)
    {
        return $this->limit = $limit;
    }
    // End setLimit

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value of the property
     *
     * @param  DOMElement $DOMElement
     * @param  \stdClass $DataObject
     * @return DOMElement
     */
    public function build(DOMDocument $Document)
    {
        $UlElt = $Document->createElement('ul');
        $UlElt->setAttribute('class', trim('htmlLimiter '. $this->getAttribute('class')));

        if($id = $this->getAttribute('id'))
            $UlElt->setAttribute('id', $id);

        foreach($this->limits as $limit)
        {
            $LiElt = $UlElt->appendChild($Document->createElement('li'));

            if($this->limit == $limit)
                $LiElt->setAttribute('class', 'active');

            $AElt = $LiElt->appendChild($Document->createElement('a', $limit));
            $AElt->setAttribute('href', (string)Url::factory($this->actionLink, ['limit' => $limit]));
        }

        return $UlElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlPager
