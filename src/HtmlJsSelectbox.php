<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;
use Beibob\HtmlTools\Interfaces\Converter;
use DOMDocument;
use Beibob\Blibs\DbObject;
use Beibob\Blibs\CssLoader;
use Beibob\Blibs\JsLoader;
use Beibob\Blibs\IdFactory;

/**
 * Baut einen JS-basierte Selectbox
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class HtmlJsSelectbox extends HtmlFormElement
{
    /**
     * uniqueId
     */
    protected $uniqueId = null;

    /**
     * Name der Box, entspricht dem Namen der Variable im Request
     */
    private $name;

    /**
     * Readonly-Flag
     * @todo - Das readonly-Feature muss noch gebaut werden.
     */
    protected $readonly = false;

    /**
     * FormName
     */
    private $formName;

    /**
     * Options
     */
    private $options = [];

    /**
     * Voreinstellung f�r Ausblendungs-Timeout
     */
    const DEFAULT_DISPLAY_TIMEOUT = 500;

    /**
     * Display timeout
     */
    private $displayTimeout;

    /**
     *
     */
    private $submitOnChange = false;

    /**
     *
     */
    private $selectedValue = null;

    /**
     * @param string               $name
     * @param null                 $value
     * @param bool                 $readonly
     * @param Interfaces\Converter $DefaultConverter
     * @param DbObject             $DataObject
     */
    public function __construct($name, $value = null, $readonly = false, Converter $DefaultConverter = null, DbObject $DataObject = null)
    {
        CssLoader::getInstance()->register('htmlTools', 'html_selectbox.css');
        JsLoader::getInstance()->register('htmlTools', 'Selectbox.js');
        $this->uniqueId = 'x'.IdFactory::getUniqueId();

        parent::__construct($name, $value, $readonly, $DefaultConverter, $DataObject);
//         $this->setName($name);
//         $this->setSubmitOnChange($submitOnChange);
//         $this->selectedValue = $selectedValue;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt den Namen des Formulars, das abgeschickt werden soll
     */
    public function setFormname($formName)
    {
        $this->formName = $formName;
    }
    // End setFormname

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt die Zeit die die Items angezeigt bleiben nachdem man die Maus aus der Box
     * bewegt hat.
     */
    public function setDisplayTimeout($milliseconds)
    {
        $this->displayTimeout = $milliseconds;
    }
    // End setDisplayTimeout

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Setzt das Flag das entscheidet ob die Selectbox das Form abschickt wenn man ein
     * Option w�hlt
     */
    public function setSubmitOnChange($submitOnChange = true)
    {
        $this->submitOnChange = $submitOnChange;
    }
    // End setSubmitOnChange

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * M�glichkeit die  Box in den Readonly-Modus zu setzen
     */
    public function setReadonly($readonly = true)
    {
        $this->readonly = $readonly;
    }
    // End setReadonly

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liefert den Status des readonly-Flags
     */
    public function getReadonly()
    {
        return $this->readonly;
    }
    // End getReadonly

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Markiert eine Option als selected, deren Wert mit dem
     * in value �bergebenen Wert �bereinstimmt
     */
    public function setSelectedByValue($value)
    {
        foreach ($this->options as $Option)
            $Option->selected = false;

        foreach ($this->options as $Option)
        {
            if ($Option->value == $value)
                $Option->selected = true;
        }
    }
    // End setSelectedByValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Erwartet eine Liste von Standardobjekten wie addOption
     */
    public function setOptions(array $options)
    {
        $this->options = [];
        foreach ($options as $Option)
            $this->options[$Option->value] = $Option;
    }
    // End setOptions

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds an selectbox option
     *
     * @param  string $name
     * @param  string $value
     * @param  string $cssClass
     * @param  bool   $selected
     * @return -
     */
    public function addOption($value, $caption, $cssClass = false, $selected = false)
    {
        $Obj = new \stdClass();
        $Obj->value    = $value;
        $Obj->caption  = $caption;
        $Obj->cssClass = $cssClass;
        $Obj->selected = $selected;

        $this->options[$value] = $Obj;
    }
    // End addOption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function build(DOMDocument $Document)
    {
        $ContainerElt = $Document->createElement('div');
        $ContainerElt->setAttribute('id', $this->getName() . $this->uniqueId);
        $ContainerElt->setAttribute('class', 'blibsHtmlSelectboxHolder');
        foreach($this->getAttributes() as $name => $value)
            $ContainerElt->setAttribute($name, $value);

        $scriptCode = [];
        $scriptCode[] = '$(document).ready(function() { ';
        $scriptCode[] = sprintf('var %sSelectbox = new blibs.html.Selectbox("%s","%s","%s", %s, %s);'
                                , $this->getName() . $this->uniqueId
                                , $this->getName() . $this->uniqueId
                                , $this->formName
                                , $this->getName()
                                , $this->displayTimeout? $this->displayTimeout : self::DEFAULT_DISPLAY_TIMEOUT
                                , $this->submitOnChange ? 'true' : 'false'
                                );

        foreach($this->options as $Option)
        {
            if (isset($Option->selected) && $Option->selected === true)
                $selected = true;
            elseif (is_null($this->selectedValue))
                $selected = false;
            else
                $selected = $this->selectedValue === $Option->value ? true : false;

            $scriptCode[] = sprintf('%sSelectbox.addOption("%s","%s","%s", %s);'
                                    , $this->getName() . $this->uniqueId
                                    , $Option->value
                                    , $Option->caption
                                    , $Option->cssClass
                                    , $selected ? 'true' : 'false'
                                    );
        }
        $scriptCode[] = '});';

        $Script = $ContainerElt->appendChild($Document->createElement('script'));
        $Script->setAttribute('type', 'text/javascript');
        $Script->appendChild($Document->createTextNode(join("\n", $scriptCode)));

        return $ContainerElt;
    }
    // End appendTo

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlJsSelectbox
?>
