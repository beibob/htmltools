<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 * This is a simple converter which simply converts an object and a property to the
 * value given by object->$property.
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class ObjectPropertyConverter extends AbstractConverter
{
    /**
     * Returns $DataObject->$property
     *
     * @param  string $value         the previous value
     * @param  \stdClass $DataObject  the data object
     * @param  string $property      the property
     * @return string                $DataObject->$property or $property if $DataObject is null
     */
    public function convertToText($value, $DataObject = null, $property = null)
    {
        if (!empty($value) || is_null($DataObject))
            return $value;

        if($property && preg_match('/^(.+?)\[(.+?)\]$/', $property, $matches))
        {
            $name = $matches[1];
            $key  = $matches[2];

            if(isset($DataObject->$name) && is_array($DataObject->$name))
            {
                $value = $DataObject->$name;
                return isset($value[$key])? $value[$key] : null;
            }
            return null;
        }

        return isset($DataObject->$property)? $DataObject->$property : null;
    }
    // End convertToText

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets $DataObject->$property to $value
     *
     * @param  string $value         the value
     * @param  \stdClass $DataObject  the data object
     * @param  string $property      the property
     * @return string                the $value
     */
    public function convertFromText($value, $DataObject = null, $property = null)
    {
        if($property && preg_match('/^(.+?)\[(.+?)\]$/', $property, $matches))
        {
            $name = $matches[1];
            $key  = $matches[2];

            if(isset($DataObject->$name) && is_array($DataObject->$name))
            {
                $value = $DataObject->$name;
                return isset($value[$key])? $value[$key] : null;
            }
            return null;
        }

        return $DataObject->$property = $value;
    }
    // convertFromText

    //////////////////////////////////////////////////////////////////////////////////////

}
// End ObjectPropertyConverter
