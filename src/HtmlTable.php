<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * This element implements a html table
 *
 * Usage example:
 *
 * <code>
 *   $Table = new HtmlTable('cds');
 *   $CDConverter = new CDConverter();
 *   $Table->addColumn('chart_position');
 *   $Table->addColumn('interpret');
 *   $Table->addColumn('song_name');
 *
 *   $Body = $Table->createTableBody();
 *   $Row = $Body->addTableRow();
 *   $Row->getColumn('chart_position')->setOutputElement(new HtmlText('position', $CDConverter));
 *   $Row->getColumn('interpret')->setOutputElement(new HtmlText('interpret', $CDConverter));
 *   $Row->getColumn('song_name')->setOutputElement(new HtmlText('name', $CDConverter));
 *   $Row->addAttrFormatter(new ZebraRowFormatter(array('odd', 'even')));
 *
 *   $Body->setDataSet($CDSet);
 *
 *   $Table->appendTo($DOMElt);
 * </code>
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlTable extends HtmlElement
{
    /**
     * Table columns
     */
    private $columns = [];

    /**
     * Table head
     */
    private $TableHead;

    /**
     * Table foot
     */
    private $TableFoot;

    /**
     * Table bodies
     */
    private $tableBodies = [];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new html table
     *
     * @param  string $tableName   the name of the table is used as the class attribute
     */
    public function __construct($cssClass)
    {
        $this->setAttribute('class', $cssClass);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a column to the table
     *
     * @param  string $name      the identifier of the column
     * @param  string $caption   the caption of the column
     * @return HtmlTableColumn   the just added column
     */
    public function addColumn($name, $caption = null)
    {
        return $this->columns[$name] = new HtmlTableColumn($name, $caption);
    }
    // End addColumn

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the columns
     *
     * @return array the columns of this table
     */
    public function getColumns()
    {
        return $this->columns;
    }
    // End getColumns

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the list of children
     *
     * @return array - the children of this element
     */
    public function getChildren() {
        return $this->tableBodies;
    }
    // End getChildren

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates the table head
     *
     * @return HtmlTableHead the table head element
     */
    public function createTableHead()
    {
        return $this->TableHead = new HtmlTableHead($this);
    }
    // End createTableHead

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds the table foot
     *
     * @return HtmlTableFoot the table footer element
     */
    public function createTableFoot()
    {
        return $this->TableFoot = new HtmlTableFoot($this);
    }
    // End createTableFoot

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a new table body
     *
     * @return HtmlTableBody the just added body
     */
    public function createTableBody()
    {
        return $this->tableBodies[] = new HtmlTableBody($this);
    }
    // End createTableBody

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the body specified by index
     *
     * @param  int $index
     * @return HtmlTableBody
     */
    public function getTableBody($index = 0)
    {
        if(!isset($this->tableBodies[$index]))
            return null;

        return $this->tableBodies[$index];
    }
    // End getTableBody

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $DOMDocument)
    {
        $TableElt = $this->getTable($DOMDocument);

        if(!is_null($this->TableHead))
            $this->TableHead->appendTo($TableElt);

        foreach($this->tableBodies as $TableBody)
            $TableBody->appendTo($TableElt);

        if(!is_null($this->TableFoot))
            $this->TableFoot->appendTo($TableElt);

        return $TableElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
    // protected
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Build the table Element and returns it as a DOMNode
     *
     * @param  DOMDocument $Document   the document to use to build the table
     * @return DOMNode                 the built table
     */
    protected function getTable(DOMDocument $Document)
    {
        $TableElt = $Document->createElement('table');
        $TableElt->setAttribute('cellspacing', 0);

        foreach($this->getAttributes() as $attr => $value)
            $TableElt->setAttribute($attr, utf8_encode($value));

        $ColGroupElt = $TableElt->appendChild($Document->createElement('colgroup'));

        foreach($this->columns as $name => $column)
        {
            $ColElt = $ColGroupElt->appendChild($Document->createElement('col'));
            $ColElt->setAttribute('class', utf8_encode($name));
        }

        return $TableElt;
    }
    // End getTable

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlTable
