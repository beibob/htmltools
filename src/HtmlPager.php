<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * NOT YET FUNCTIONAL!!!
 * @ignore
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class HtmlPager extends HtmlElement
{
    /**
     * Default count of displayed pages
     */
    const DEFAULT_PAGE_DISPLAY_COUNT = 7;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * ActionLink
     */
    private $actionLink;

    /**
     * Page count
     */
    private $pageCount = 1;

    /**
     * Page index
     */
    private $pageIndex = 1;

    /**
     * Page display count
     */
    private $pageDisplayCount = self::DEFAULT_PAGE_DISPLAY_COUNT;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the action links
     *
     * @param  string $actionLink
     * @return -
     */
    public function setActionLink($link)
    {
        return $this->actionLink = $link;
    }
    // End setActionLink

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the page count
     *
     * @param  integer $pageCount
     * @return integer
     */
    public function setPageCount($pageCount)
    {
        return $this->pageCount = $pageCount;
    }
    // End setPageCount

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the page index
     *
     * @param  integer $pageIndex
     * @return integer
     */
    public function setPageIndex($pageIndex)
    {
        return $this->pageIndex = abs($pageIndex);
    }
    // End setPageIndex

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the page index maximum
     *
     * @param  integer $displayCount
     * @return integer
     */
    public function setPageDisplayCount($displayCount = self::DEFAULT_PAGE_DISPLAY_COUNT)
    {
        return $this->pageDisplayCount = $displayCount;
    }
    // End setPageDisplayCount

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the value of the property
     *
     * @param  DOMElement $DOMElement
     * @param  \stdClass $DataObject
     * @return DOMElement
     */
    public function build(DOMDocument $Document)
    {
        /**
         * Hide pager if page count <= 1
         */
        if($this->pageCount <= 1)
            return $Document->createTextNode('');

        $UlElt = $Document->createElement('ul');
        $UlElt->setAttribute('class', trim('htmlPager '. $this->getAttribute('class')));

        if($id = $this->getAttribute('id'))
            $UlElt->setAttribute('id', $id);

        // begin
        $LiElt = $UlElt->appendChild($Document->createElement('li'));
        $LiElt->setAttribute('class', 'nav');

        $AElt = $LiElt->appendChild($Document->createElement('a', '<<'));
        $AElt->setAttribute('href', (string)Url::factory($this->actionLink, ['index' => 1]));

        // previous
        $LiElt = $UlElt->appendChild($Document->createElement('li'));
        $LiElt->setAttribute('class', 'nav');

        $prevIndex = $this->pageIndex > 1? $this->pageIndex - 1 : 1;
        $AElt = $LiElt->appendChild($Document->createElement('a', '<'));
        $AElt->setAttribute('href', (string)Url::factory($this->actionLink, ['index' => $prevIndex]));

        // page index
        $startIndex = $this->pageIndex >= $this->pageDisplayCount? min($this->pageIndex - ceil($this->pageDisplayCount / 2) + 1, $this->pageCount - $this->pageDisplayCount + 1) : 1;
        $stopIndex  = $this->pageIndex >= $this->pageDisplayCount? $startIndex + $this->pageDisplayCount : min($this->pageCount, $this->pageDisplayCount) + 1;

        for($i = $startIndex; $i < $stopIndex; $i++)
        {
            $LiElt = $UlElt->appendChild($Document->createElement('li'));

            if($this->pageIndex == $i)
                $LiElt->setAttribute('class', 'active');

            $AElt = $LiElt->appendChild($Document->createElement('a', $i));
            $AElt->setAttribute('href', (string)Url::factory($this->actionLink, ['index' => $i]));
        }

        // goto next
        $LiElt = $UlElt->appendChild($Document->createElement('li'));
        $LiElt->setAttribute('class', 'nav');

        $nextIndex = $this->pageIndex < $this->pageCount? $this->pageIndex + 1 : $this->pageCount;
        $AElt = $LiElt->appendChild($Document->createElement('a', '>'));
        $AElt->setAttribute('href', (string)Url::factory($this->actionLink, ['index' => $nextIndex]));

        // end
        $LiElt = $UlElt->appendChild($Document->createElement('li'));
        $LiElt->setAttribute('class', 'nav');

        $AElt = $LiElt->appendChild($Document->createElement('a', '>>'));
        $AElt->setAttribute('href', (string)Url::factory($this->actionLink, ['index' => $this->pageCount]));

        return $UlElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlPager
