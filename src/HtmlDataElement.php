<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\DbObject;
use Beibob\HtmlTools\Interfaces\Converter;


/**
 * Base class for all HTML data elements.
 *
 * A data element is classified by having a name property and
 * a DataObject, which both lead to an object->property accessor.
 *
 * This element ist widely used in a read-only fashion, for a read-write
 * extension see HtmlEditableElement, which is the base class for this case
 * and a direct derivation of HtmlDataElement.
 *
 * This class also features a converter chain. You can register Converters
 * here, that are used to convert the DataObject's property to a text value.
 *
 * The default value for the converterChain is a single ObjectPropertyConverter.
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 * @abstract
 *
 */
abstract class HtmlDataElement extends HtmlElement
{
    /**
     * List of converters
     */
    private $converterChain = [];

    /**
     * The data object
     */
    private $DataObject;

    /**
     * The name
     */
    private $name;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new HtmlDataObject
     *
     * If no default converter is given or null, a standard ObjectPropertyConverter is added
     * to the chain.
     *
     * @param string $name                   the name of the object (also known as property)
     * @param Converter $defaultConverter    the default converter to be used
     * @param DbObject $DataObject           the data object to use
     */
    public function __construct($name, Converter $defaultConverter = null, DbObject $DataObject = null)
    {
        $this->name = $name;
        if ($defaultConverter != null)
            $this->converterChain[] = $defaultConverter;
        else
            $this->converterChain[] = new ObjectPropertyConverter();

        $this->DataObject = $DataObject;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the raw text value after calling the converter chain
     *
     * @return string    the text value
     */
    public function getConvertedTextValue($value = null)
    {
        foreach ($this->converterChain as $Converter)
            $value = $Converter->convertToText($value, $this->DataObject, $this->name);

        return $value;
    }
    // End getConvertedTextValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the data object
     *
     * @param  \stdClass $DataObject
     */
    public function setDataObject($DataObject = null)
    {
        $this->DataObject = $DataObject;
    }
    // End setDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the data object recursive on all children that are HtmlDataElements
     *
     * @param  HtmlElement $Element   The html element to start with
     * @param  \stdClass $DataObject   The data object to set recursive
     */
    public static function setDataObjectRecursive(HtmlElement $Element, $DataObject = null)
    {
        foreach($Element->getChildren() as $Child)
        {
            if($Child instanceof HtmlDataElement)
                $Child->setDataObject($DataObject);

            self::setDataObjectRecursive($Child, $DataObject);
        }
    }
    // End setDataObjectRecursive

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the data object
     *
     * @return \stdClass    the data object
     */
    public function getDataObject() {
        return $this->DataObject;
    }
    // End getDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns true if the DataObject isset
     *
     * @return boolean
     */
    public function hasDataObject()
    {
        return is_object($this->DataObject);
    }
    // End getDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the name
     *
     * @param  string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    // End setName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the name
     *
     * @return string    the name
     */
    public function getName()
    {
        return $this->name;
    }
    // End getName

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Clears the converter chain
     */
    public function clearConverters() {
        $this->converterChain = [];
    }
    // End clearConverters

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds a converter to the converter chain
     *
     * @param  Converter $Converter    the Converter to add to the end of the chain
     */
    public function addConverter(Converter $Converter) {
        $this->converterChain[] = $Converter;
    }
    // End addConverter

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Calls the converter chain backwards and returns the data object
     *
     * @param mixed $value    the value to use as a start
     * @return DbObject       the validated data object
     */
    public function getValidatedFormDataObjectForValue($value = null)
    {
        foreach ($this->converterChain as $Converter) {
            $value = $Converter->convertFromText($value, $this->getDataObject(), $this->getName());
        }
        return $this->getDataObject();
    }
    // End getValidatedFormDataObjectForValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Calls the converter chain backwards and returns the data object
     *
     * @param mixed $value    the value to use as a start
     * @return DbObject       the validated data object
     */
    public function getValidatedFormDataObject() {
        return $this->getValidatedFormDataObjectForValue();
    }
    // End getValidatedFormDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Calls getValidatedFormDataObject recursively
     *
     * @param HtmlElement $Element  the element to call the function on
     */
    public static function recursiveGetValidatedFormDataObject(HtmlElement $Element)
    {
        foreach ($Element->getChildren() as $Child) {
            if ($Child instanceof HtmlDataElement)
                $Child->getValidatedFormDataObject();
            self::recursiveGetValidatedFormDataObject($Child);
        }
    }
    // End recursiveGetValidatedFormDataObject

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * This function returns the first child in the children tree of $Element which
     * is of the type HtmlFormElement.
     *
     * @param HtmlElement $Element   the element to search
     * @return HtmlDataElement       or null
     **/
    public static function searchFirstDataElementChild(HtmlElement $Element) {
        foreach ($Element->getChildren() as $Child) {
            if ($Child instanceof HtmlDataElement)
                return $Child;

            $Ret = self::searchFirstDataElementChild($Child);
            if (!is_null($Ret))
                return $Ret;
        }
        return null;
    }
}
// End HtmlDataElement
