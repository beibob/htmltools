<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

/**
 * Baut ein onclick
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 * @abstract
 *
 */
class LoadOnclickFormater extends AbstractFormatter
{
    /**
     * An internal counter
     */
    protected $url;
    protected $property = 'id';
    protected $requestName = 'requestName';

    /**
     * Creates a new instance of the zebra row formatter
     *
     * @param array $url
     * @param string $property
     */
    public function __construct($url, $property = 'id', $requestName = 'id')
    {
        $this->url = $url;
        $this->property = $property;
        $this->requestName = $requestName;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Formats the object
     *
     * @see Formatter::format()
     */
    public function format(HtmlElement $obj, $DataObject = null, $property = null)
    {
        $propertyName = $this->property;
        $obj->setAttribute('onclick', sprintf("location.href='%s?%s=%s';", $this->url, $this->requestName, $DataObject->$propertyName));
    }
    // End format
}
// End LoadOnclickFormater
?>
