<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DateTime;


/**
 * Converts a unix timestamp into a date
 * just another front end to php's date function
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 * @author     Fabian M�ller <fab@beibob.de>
 * @author     Thorsten M�rell <thorsten.muerell@beibob.net>
 *
 */
class DateTimeConverter extends ObjectPropertyConverter
{
    /**
     * Default parameters
     */
    protected $defaultParameters = array('format' => 'd.m.Y, H:i');

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the converted value
     *
     * @see Converter::convertToText()
     */
    public function convertToText($value, $DataObject = null, $property = null)
    {
        $value = parent::convertToText($value, $DataObject, $property);

        if(is_string($value))
            $DT = new DateTime($value);
        elseif($value instanceof \DateTimeInterface)
            $DT = $value;
        else
            return null;

        return $DT->format($this->get('format'));
    }
    // End convertToText

    //////////////////////////////////////////////////////////////////////////////////////
}
// End DateTimeConverter
