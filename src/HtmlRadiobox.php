<?php
/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\IdFactory;
use DOMDocument;
use Verraes\ClassFunctions\ClassFunctions;

/**
 * This implements a simple radiobox for usage in HtmlRadioGroups
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlRadiobox extends HtmlFormElement
{
    /**
     * The caption of the radiobox
     */
    private $caption;
    /**
     * The value of the radiobox
     */
    private $value;
    /**
     * The radio group this radiobox belongs to
     */
    private $RadioGroup;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new Radiobox element
     *
     * @param string $caption  the caption of the radiobox
     * @param string $value    the value of the radiobox
     */
    public function __construct($caption, $value) {
        $this->caption = $caption;
        $this->value = $value;
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the caption
     *
     * @param  string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }
    // End setCaption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the value
     *
     * @param  string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
    // End setValue

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the associated radio group
     *
     * @param  HtmlRadioGroup $RadioGroup the html radio group to associate
     */
    public function setRadioGroup(HtmlRadioGroup $RadioGroup)
    {
        $this->RadioGroup = $RadioGroup;
    }
    // End setRadioGroup

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the associated radio group
     *
     * @return HtmlRadioGroup the radio group
     */
    public function getRadioGroup()
    {
        return $this->RadioGroup;
    }
    // End getRadioGroup

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $DivElt = $Document->createElement('div');

        $InputElt = $Document->createElement('input');
        $InputElt->setAttribute('type', 'radio');
        $InputElt->setAttribute('name', $this->RadioGroup->getName());
        $InputElt->setAttribute('value', $this->value);
        $InputElt->setAttribute('id', IdFactory::getUniqueXmlId());

        $value = $this->RadioGroup->getConvertedTextValue();

        if ($this->value == $value || $this->getAttribute('checked') === 'checked') {
            $InputElt->setAttribute('checked', 'checked');
        }

        if ($this->isReadonly()) {
            $InputElt->setAttribute('readonly', 'readonly');
        }
        if ($this->isDisabled()) {
            $InputElt->setAttribute('disabled', 'disabled');
        }

        $DivElt->appendChild($InputElt);

        $LabelElt = $Document->createElement('label');
        $LabelElt->setAttribute('for', $InputElt->getAttribute('id'));
        $LabelElt->appendChild($Document->createTextnode($this->caption));

        $DivElt->appendChild($LabelElt);

        $this->buildAndSetAttributes($DivElt, $this->getDataObject(), $this->getName());
        $DivElt->setAttribute('class', $DivElt->getAttribute('class') .' '.ClassFunctions::short($this));


        return $DivElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

    // Disable this for a radio box
    public function getValidatedFormDataObject() { return null; }

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlRadiobox
