<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\Validator;

/**
 * Validates a HtmlForm
 *
 * @package blibs
 * @author Tobias Lode <tobias@beibob.de>
 */
class HtmlFormValidator extends Validator
{
    /**
     * Returns the value for the property
     *
     * @param  string $property
     * @return string
     */
    public function getValue($property, $value = null)
    {
        if(!is_null($value) || !is_object($this->DataObject))
            return $value;

        /**
         * Check for array properties
         */
        if(!preg_match('/^(.+?)\[(.+?)\]$/', $property, $matches))
            return isset($this->DataObject->$property)? $this->DataObject->$property : null;

        $name = $matches[1];
        $key  = $matches[2];

        if(!isset($this->DataObject->$name) || !is_array($this->DataObject->$name))
            return null;

        $value = $this->DataObject->$name;
        return isset($value[$key])? $value[$key] : null;
    }
    // End setData

    //////////////////////////////////////////////////////////////////////////////////////

}
// End HtmlFormValidator
