<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;


/**
 * Implements a form submit button.
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 * @author Tobias Lode <tobias@beibob.de>
 * @author Fabian Möller <fab@beibob.de>
 *
 */
class HtmlButton extends HtmlInputElement
{
    /**
     *
     */
    protected $caption;

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new html submit button
     *
     * @param string $name    the name of the button
     * @param string $value   the value (i.e. the caption of the button)
     */
    public function __construct($name, $caption, $onClick = 'this.form.submit();', $type = 'button', $value = null, $readonly = false)
    {
        parent::__construct($name, $value, $readonly);
        $this->setCaption($caption);
        $this->setType($type);
        if ($onClick)
            $this->setAttribute('onclick', $onClick);
    }
    // End __construct

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }
    // End setCaption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     */
    public function getCaption()
    {
        return $this->caption;
    }
    // End setCaption

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $ButtonElt = $Document->createElement('button');
        $ButtonElt->setAttribute('type', $this->getType());
        $ButtonElt->setAttribute('name', $this->getName());
        if (!is_null($this->getValue()))
            $ButtonElt->setAttribute('value', $this->getConvertedTextValue());

        if ($this->isReadonly())
            $ButtonElt->setAttribute('readonly', 'readonly');

        $this->buildAndSetAttributes($ButtonElt, $this->getDataObject(), $this->getName());

        $CaptionDiv = $Document->createElement('div');
        $Span = $CaptionDiv->appendChild($Document->createElement('span'));
        $Span = $Span->appendChild($Document->createElement('span'));
        $Span->appendChild($Document->createTextNode($this->getCaption()));
        $ButtonElt->appendChild($CaptionDiv);

        return $ButtonElt;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////

}
// End HtmlButton
