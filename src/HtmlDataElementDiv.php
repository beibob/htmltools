<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use DOMDocument;

/**
 * An element which creates a HTML link.
 *
 * The attribute href of the element is set to the value of the
 * getConvertedTextValue call of HtmlDataElement.
 *
 * The content is build from the children.
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlDataElementDiv extends HtmlDataElement
{
    /**
     * Builds this element.
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document)
    {
        $Div = $Document->createElement('div');

        $this->buildAndSetAttributes($Div, $this->getDataObject(), $this->getName());

        foreach ($this->getChildren() as $Child) {
            $Child->appendTo($Div);
        }

        return $Div;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlDataElementDiv
