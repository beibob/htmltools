<?php
/**
 * This file is part of blibs - mvc development framework
 * 
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 * 
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Beibob\HtmlTools;

use Beibob\Blibs\CssLoader;
use Beibob\Blibs\DbObjectSet;
use Beibob\Blibs\JsLoader;
use DOMDocument;

/**
 *
 * @package blibs
 * @author Thorsten Mürell <thorsten.muerell@beibob.net>
 *
 */
class HtmlSelectboxChooser extends HtmlFormElement
{
    /**
     * Src and dst sets
     */
    private $SrcSet;
    private $DstSet;

    /**
     * Properties Set
     */

    private $keyProperty ;
    private $captionProperty;
    
    /**
     * captions
     */
    private $srcCaption;
    private $dstCaption;

    /**
     *
     * @param <type> $keyProperty //value of the select option
     * @param <type> $captionProperty // Caption of the select option
     */
    public function __construct( $keyProperty , $captionProperty)
    {
        $this->setProperties( $keyProperty , $captionProperty );
        CssLoader::getInstance()->register('htmlTools', 'selectboxChooser.css');
        JsLoader::getInstance()->register('htmlTools','selectboxChooser.js');
    }
    //End __construct
    
    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the source and the destination DbObjectSet
     *
     * @param  DbObjectSet SrcSet
     * @param  DbObjectSet DestSet
     * @return -
     */
    public function setDbObjectSets(DbObjectSet $SrcSet, DbObjectSet $DstSet)
    {
        $this->DstSet = $DstSet;
        $this->SrcSet = $SrcSet;
    }
    // End setDbObjectSets

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param <type> $keyProperty
     * @param <type> $captionProperty
     */
    public function setProperties( $keyProperty , $captionProperty )
    {
        $this->captionProperty = $captionProperty;
        $this->keyProperty = $keyProperty;
    }
    //End setProperties

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the source and the destination captions
     *
     * @param  string $srcCaption
     * @param  string $dstCaption
     * @return -
     */
    public function setCaptions($srcCaption, $dstCaption)
    {
        $this->dstCaption = $dstCaption;
        $this->srcCaption = $srcCaption;
    }
    // End setCaptions

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Builds this element
     *
     * @see HtmlElement::build()
     */
    public function build(DOMDocument $Document )
    {


            //Div for administration
            $SelectboxChooserElt = new HtmlTag('div');
            $SelectboxChooserElt->setAttribute('class','administration');

            //Builds the source div
            $srcDiv = $SelectboxChooserElt->add(new HtmlTag('div',$this->srcCaption));
            $srcDiv->setAttribute('class','allSrc');
            $srcSelectBox = $srcDiv->add(new HtmlSelectbox('src[]'));
            $srcSelectBox->setAttribute('class','srcSelectBox');
            $srcSelectBox->setAttribute('multiple','multiple');
            
            //adds the options in the source div's selectbox
            $allSrcs = [];
            foreach ($this->SrcSet as $DbObject )
            {
                $captionProperty = $this->captionProperty;
                $keyProperty = $this->keyProperty;
                
                $srcSelectBox->add(new HtmlSelectOption($DbObject->$captionProperty,$DbObject->$keyProperty));
                    $allSrcs = $allSrcs[$DbObject->$keyProperty] = $DbObject->$captionProperty;
            }

            //button Div for move right or move left
            $middle = $SelectboxChooserElt->add(new HtmlTag('div'));
            $middle->setAttribute('class','middle');
            $rightButton = $middle->add(new HtmlButton('right','>>','addSelected()'));
            $rightButton->setAttribute('class','rightButton');
            $leftButton = $middle->add(new HtmlButton('left','<<','removeSelected()'));
            $leftButton->setAttribute('class','leftButton');

            //hidden Input 1
            $hiddenInput = $SelectboxChooserElt->add(new HtmlTextInput('added'));
            $hiddenInput->setAttribute('type','hidden');
            $hiddenInput->setAttribute('id','added');

            $hiddenInput2 = $SelectboxChooserElt->add(new HtmlTextInput('removed'));
            $hiddenInput2->setAttribute('type','hidden');
            $hiddenInput2->setAttribute('id','removed');

            //Builds the destination div
            $dstDiv = $SelectboxChooserElt->add(new HtmlTag('div',$this->dstCaption));
            $dstDiv->setAttribute('class','allDst');
            $dstSelectBox = $dstDiv->add(new HtmlSelectbox('dst[]'));
            $dstSelectBox->setAttribute('class','dstSelectBox');
            $dstSelectBox->setAttribute('multiple','multiple');

            //adds the options in the destanation div's selectbox
            $allSrcs = [];
            foreach ($this->DstSet as $DbObject )
            {
                $captionProperty = $this->captionProperty;
                $keyProperty = $this->keyProperty;
                
                $dstSelectBox->add(new HtmlSelectOption($DbObject->$captionProperty,$DbObject->$keyProperty));
                    $allDsts = $allDsts[$DbObject->$keyProperty] = $DbObject->$captionProperty;
            }

            $selectectBoxChooserDiv = $Document->createElement('div');
            $selectectBoxChooserDiv->setAttribute('class','selectectBoxChooser');
            $SelectboxChooserElt->appendTo($selectectBoxChooserDiv);
            return $selectectBoxChooserDiv;
    }
    // End build

    //////////////////////////////////////////////////////////////////////////////////////
}
// End HtmlSelectboxChooser
