/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
$(document).ready(function() {

/**
 * Invertieren aller Checkboxen der Tabelle
 */
$('#multiselectInvert').click(function() {
  $(".multiselectCheckbox").each(function() {
    if ($(this).attr('checked'))
        $(this).removeAttr('checked');
    else
        $(this).attr('checked', 'checked');
  });
});

/**
 * Alle Checkboxen der Tabelle auswählen
 */
$('#multiselectAll').click(function() {
  if ($(this).hasClass('hi'))
  {
    $(this).removeClass('hi');
    $(this).addClass('lo');

    $(".multiselectCheckbox").each(function() {
      $(this).attr('checked', 'checked');
    });
  }
  else
  {
    $(this).removeClass('lo');
    $(this).addClass('hi');

    $(".multiselectCheckbox").each(function() {
      $(this).removeAttr('checked');
    });
  }
});

});