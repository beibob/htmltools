/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
var blibs;
if(!blibs) blibs = {};
if(!blibs.html) blibs.html = {};

////////////////////////////////////////////////////////////////////////////////
// Selectbox - builds a html selectbox
//
// formName    - name of form
// containerId - id of container holding this selectbox
// displayTimeout - time in ms to display the options container
//
////////////////////////////////////////////////////////////////////////////////

blibs.html.Selectbox = function(containerId, formName, elementName, displayTimeout, submitOnChange)
{
    this.containerId = containerId;
    this.formName = formName;
    if (this.formName == '')
    {
        Formhelper = new blibs.FormHelper();
        this.formName = Formhelper.getParentFormByElement(document.getElementById(containerId)).name;
    }
    this.elementName = elementName;
    this.displayTimeout = displayTimeout? displayTimeout : 500;
    this.submitOnChange = submitOnChange;

    this.options      = new Array();
    this.isExpanded   = false;

    ////////////////////////////////////////////////////////////////////////////////

    // constructor method - builds the selectbox
    this._build = function()
    {
        var containerElt = document.getElementById(this.containerId);

        var selectboxElt = containerElt.appendChild(document.createElement('div'));
        selectboxElt.className = 'blibsHtmlSelectbox';
        selectboxElt.onmouseover = this._onMouseOver.bindEventListener(this);
        selectboxElt.onmouseout  = this._onMouseOut.bindEventListener(this);
        selectboxElt.onclick     = this._onClick.bindEventListener(this);

        this.selectedOptionContainerElt = selectboxElt.appendChild(document.createElement('div'));
        this.selectedOptionContainerElt.className = 'selectedOption';
        this.optionsWrapperElt = selectboxElt.appendChild(document.createElement('div'));
        this.optionsWrapperElt.className = 'optionsWrapper';
        this.optionsWrapperElt.onclick = this._onClick.bindEventListener(this);

        this.optionsHolderElt = this.optionsWrapperElt.appendChild(document.createElement('div'));
        this.optionsHolderElt.className = 'optionsHolder';
        this.hideOptions();
    };

    ////////////////////////////////////////////////////////////////////////////////

    // adds an option to the selectbox
    this.addOption = function(value, caption, cssclass, selected)
    {
        this.options.push(new blibs.html.SelectboxOption(this, value, caption, cssclass, selected));
    };

    ////////////////////////////////////////////////////////////////////////////////

    // sets the given value
    this.setValue = function(value)
    {
        // set form input field
        if(!document.forms[this.formName])
            return;

        if(document.forms[this.formName].elements[this.elementName])
        {
            document.forms[this.formName].elements[this.elementName].value = value;
        }
        else
        {
            var InputElt = document.createElement('input');
            InputElt.type = 'hidden';
            InputElt.name = this.elementName;
            InputElt.value = value;
            document.forms[this.formName].appendChild(InputElt);
        }
    };

    ////////////////////////////////////////////////////////////////////////////////

    // displays the options container
    this.displayOptions = function()
    {
        this.optionsWrapperElt.style.display = '';
        this._clearDisplayTimer();
        this.isExpanded = true;
    };

    ////////////////////////////////////////////////////////////////////////////////

    // hides the options container
    this.hideOptions = function()
    {
        this.optionsWrapperElt.style.display = 'none';

        this._clearDisplayTimer();
        this.isExpanded = false;
    };

    ////////////////////////////////////////////////////////////////////////////////

    // selects nothing and hides the option container
    this.selectNone = function()
    {
        for(var i in this.options)
            this.options[i].unselect();

        if(this.selectedOptionContainerElt.firstChild)
            this.selectedOptionContainerElt.removeChild(this.selectedOptionContainerElt.firstChild);

        this.hideOptions();
    };

    ////////////////////////////////////////////////////////////////////////////////

    // returns the option container element
    this.getOptionsHolder = function()
    {
        return this.optionsHolderElt;
    };

    ////////////////////////////////////////////////////////////////////////////////

    // returns the selected option container element
    this.getSelectedOptionContainer = function()
    {
        return this.selectedOptionContainerElt;
    };

    ////////////////////////////////////////////////////////////////////////////////

    // starts the display timer
    this._startDisplayTimer = function()
    {
        this.displayTimer = window.setTimeout(this.hideOptions.bind(this), this.displayTimeout);
    };

    ////////////////////////////////////////////////////////////////////////////////

    // clears the display timer
    this._clearDisplayTimer = function()
    {
        if(this.displayTimer)
            window.clearTimeout(this.displayTimer);
    };

    ////////////////////////////////////////////////////////////////////////////////

    // onclick event handler
    this._onClick = function(event)
    {
        if(!this.isExpanded)
            this.displayOptions();
        else
            this.hideOptions();
    };

    ////////////////////////////////////////////////////////////////////////////////

    // onmouseover event handler
    this._onMouseOver = function(event)
    {
        if(this.isExpanded)
            this._clearDisplayTimer();
    };

    ////////////////////////////////////////////////////////////////////////////////

    // onmouseout event handler
    this._onMouseOut = function(event)
    {
        if(this.isExpanded)
            this._startDisplayTimer();
    };

    ////////////////////////////////////////////////////////////////////////////////

    // call constructor method
    this._build()
};

////////////////////////////////////////////////////////////////////////////////
// SelectboxOption
////////////////////////////////////////////////////////////////////////////////

blibs.html.SelectboxOption = function(selectbox, value, caption, cssClass, selected)
{
    this.selectbox = selectbox;
    this.value    = value;
    this.caption  = caption;
    this.cssClass = cssClass? cssClass : this.value;
    this.selected = selected? true : false;

    ////////////////////////////////////////////////////////////////////////////////

    // constructor method
    // builds the option and appends it to the option container of the selectbox
    this._build = function(containerElt)
    {
        this.optElt = this.selectbox.getOptionsHolder().appendChild(document.createElement('div'));
        if(this.cssClass)
            this.optElt.className = 'optionItem ' + this.cssClass;
        else
            this.optElt.className = 'optionItem';

        this.optElt.appendChild(document.createTextNode(this.caption));

        if(this.selected)
            this.select();

        this.optElt.onmouseover = this._onMouseOver.bindEventListener(this);
        this.optElt.onmouseout  = this._onMouseOut.bindEventListener(this);
        this.optElt.onclick     = this._onClick.bindEventListener(this);
    };

    ////////////////////////////////////////////////////////////////////////////////

    // marks the option as selected
    this.select = function()
    {
        this.selectbox.selectNone();

        var selectedOptionElt = this.selectbox.getSelectedOptionContainer();
        selectedOptionElt.appendChild(document.createTextNode(this.optElt.firstChild.nodeValue));

        this._setHighlight();
        this.selected = true;

        this.selectbox.setValue(this.value);
    };

    ////////////////////////////////////////////////////////////////////////////////

    // unselects the option
    this.unselect = function()
    {
        this.selected = false;
        this._unsetHighlight();
    };

    ////////////////////////////////////////////////////////////////////////////////

    // highlights the option
    this._setHighlight = function()
    {
        this.optElt.className = 'optionItem ' + this.cssClass + ' highlight';
    };

    ////////////////////////////////////////////////////////////////////////////////

    // removes the highlight of the option
    this._unsetHighlight = function()
    {
        if(!this.selected)
            this.optElt.className = 'optionItem ' + this.cssClass;
    };

    ////////////////////////////////////////////////////////////////////////////////

    // onmouseover event handler
    this._onMouseOver = function(event)
    {
        this._setHighlight();
    };

    ////////////////////////////////////////////////////////////////////////////////

    // onmouseout event handler
    this._onMouseOut = function(event)
    {
        this._unsetHighlight();
    };

    ////////////////////////////////////////////////////////////////////////////////

    // onclick event handler
    this._onClick = function(event)
    {
        this.select();
        if (this.selectbox.submitOnChange)
            document.forms[this.selectbox.formName].submit();
    };

    ////////////////////////////////////////////////////////////////////////////////

    // call the constructor method
    this._build();
};

////////////////////////////////////////////////////////////////////////////////
