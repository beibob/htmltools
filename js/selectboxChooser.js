/**
 * This file is part of blibs - mvc development framework
 *
 * Copyright (c) 2013 Tobias Lode <tobias@beibob.de>
 *                    Fabian Möller <fab@beibob.de>
 *                    BEIBOB Medienfreunde GbR - http://beibob.de/
 *
 * blibs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blibs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with blibs. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author     Dakam Franck <franck@beibob.de>
 */
function addSelected()
{
    $('.srcSelectBox :selected').each(function() {
                var clone = this.cloneNode(true);
                var exist = false;
                var addedMembers = $('#added').val();
                //Checks if the option allready exists
                $('.dstSelectBox option').each(function() {
                    if(this.value == clone.value)
                            exist = true;
                });

                //add option if not exist addedMembers
                if(!exist)
                {
                    $('.dstSelectBox').append(clone);
                    if(addedMembers == '')
                        addedMembers = this.value;
                    else
                        addedMembers = addedMembers + ',' + this.value;
                    wasRemoved(this.value)
                }

                $('#added').val(addedMembers);
        });

    return false;
}

function removeSelected()
{
    var removedMembers = $('#removed').val();

    $('.dstSelectBox :selected').each(function() {
        $('.dstSelectBox :selected').remove();
        if(removedMembers == '')
            removedMembers = this.value;
        else
            removedMembers = removedMembers + ',' + this.value;

       wasAdded(this.value);

    });


    $('#removed').val(removedMembers);
}

//checks if this id was added
function wasAdded(id)
{
    var addedMembers = $('#added').val();
    var aMembers = addedMembers.split(',');
    addedMembers = '';
    if(arrayFind(id , aMembers))
    {
        for (var i=0; i < aMembers.length; i++)
        {

            if(aMembers[i] != id && aMembers[i] != null )
            {
                if(addedMembers == '')
                    addedMembers = aMembers[i];
                else
                    addedMembers = addedMembers + ',' + aMembers[i];
            }
        }
        $('#added').val(addedMembers);
   }
}

//checks if this id was removed
function wasRemoved(id)
{
    var removedMembers = $('#removed').val();
    var aMembers = removedMembers.split(',');
    removedMembers = '';
    if(arrayFind(id , aMembers))
    {
        for (var i=0; i < aMembers.length; i++)
        {

            if(aMembers[i] != id && aMembers[i] != null )
            {
                if(removedMembers == '')
                    removedMembers = aMembers[i];
                else
                    removedMembers = removedMembers + ',' + aMembers[i];
            }
        }
        $('#removed').val(removedMembers);
   }
}


function arrayFind(key , array)
{
     for (var i=0; i < array.length; i++) {
         if(key == array[i])
         {
             array[i] = null;
             return true;
         }
     }

     return false;
}

